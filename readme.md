# What is this? #
---
Wrest is a timer application for Windows that helps you take breaks from your computer. You set a work and rest duration and the timer will remind you when it's time to take a break.

# Why is this? #
---
I created Wrest for myself so that I would have an easier time remembering to take breaks. It's basically a native version of your normal [web-based pomodoro](https://tomato-timer.com/) timer.


This is a re-write of the original version which was written in [AHK](https://autohotkey.com/). This is my first project using C#, so be warned if you decide to modify the source, it's not a pretty sight.


# Pictures #
---
## Main application window ##
![Application window](docs/app_screenshot.png)


## Reminder popup ##
![Reminder popup](docs/reminder_screenshot.png)


## Settings window ##
![Settings window](docs/settings_screenshot.png)

# Common problems #
---
Q: Why is my release build crashing on startup?
A: Try removing the old settings file, it might be corrupt.

Q: How do I bump the version number?
A: Properties -> Settings.settings, change it here. Don't touch App.config.