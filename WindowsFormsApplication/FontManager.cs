﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.Runtime.InteropServices;

namespace Wrest
{
    static class FontManager
    {
        private static readonly PrivateFontCollection PRIVATE_FONT_COLLECTION = 
            new PrivateFontCollection();

        private static readonly Dictionary<string, int> FONT_ID_MAP = 
            new Dictionary<string, int>();

        static FontManager()
        {
            FONT_ID_MAP.Add("Asap", 0);

            var fontAsapBold = Properties.Resources.Asap_Bold;

            byte[] fontData = fontAsapBold;
            IntPtr fontPtr = Marshal.AllocCoTaskMem(fontData.Length);
            Marshal.Copy(fontData, 0, fontPtr, fontData.Length);
            PRIVATE_FONT_COLLECTION.AddMemoryFont(fontPtr, fontAsapBold.Length);

            uint dummy = 0;
            AddFontMemResourceEx(fontPtr, (uint)fontAsapBold.Length, IntPtr.Zero, ref dummy);

            Marshal.FreeCoTaskMem(fontPtr);
        }

        [DllImport("gdi32.dll")]
        private static extern IntPtr AddFontMemResourceEx(IntPtr pbFont, uint cbFont,
            IntPtr pdv, [In] ref uint pcFonts);

        internal static Font AsapFont(int size)
        {
            int value;
            FONT_ID_MAP.TryGetValue("Asap", out value);
            FontFamily ff = PRIVATE_FONT_COLLECTION.Families[value];

            return new Font(ff, size, FontStyle.Bold);
        }
    }
}
