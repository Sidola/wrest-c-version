﻿using System;

namespace Wrest
{
    internal class ActiveTimerTickEventArgs : EventArgs
    {
        internal TimerType TimerType { get; }
        internal long RemainingDurationMilli { get; }

        internal ActiveTimerTickEventArgs(TimerType timerType, long remainingDurationMilli)
        {
            TimerType = timerType;
            RemainingDurationMilli = remainingDurationMilli;
        }
    }
}
