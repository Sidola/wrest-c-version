﻿using System;

namespace Wrest
{
    class TimerChangedStateArgs : EventArgs
    {
        internal TimerState TimerState { get; }
        internal TimerType TimerType { get; }

        internal TimerChangedStateArgs(TimerState timerState, TimerType timerType)
        {
            TimerState = timerState;
            TimerType = timerType;
        }
    }

}
