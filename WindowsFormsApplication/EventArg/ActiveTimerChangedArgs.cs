﻿using System;

namespace Wrest
{
    class ActiveTimerChangedArgs : EventArgs
    {
        internal TimerType PreviousTimerType { get; }
        internal TimerType NextTimerType { get; }
        internal bool TimerIsStarted { get; }

        public ActiveTimerChangedArgs(bool timerIsStarted, TimerType previousTimerType, TimerType nextTimerType)
        {
            TimerIsStarted = timerIsStarted;
            PreviousTimerType = previousTimerType;
            NextTimerType = nextTimerType;
        }

    }
}
