﻿using System;

namespace Wrest
{
    public class TimerChangingEventArgs : EventArgs
    {
        public TimerType TimerType { get; }

        public TimerChangingEventArgs(TimerType timerType)
        {
            TimerType = timerType;
        }

    }
}
