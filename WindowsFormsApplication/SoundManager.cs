﻿using System;
using System.IO;
using System.Media;

namespace Wrest
{
    internal class SoundManager
    {
        internal SoundPlayer workSoundPlayer;
        internal SoundPlayer restSoundPlayer;

        internal SoundManager()
        {
            Stream str = Properties.Resources.to_work;
            workSoundPlayer = new SoundPlayer(Properties.Resources.to_work);
            restSoundPlayer = new SoundPlayer(Properties.Resources.to_rest);
        }

        internal void PlaySwitchSound(TimerType timerType)
        {
            if (timerType == TimerType.Work)
            {
                workSoundPlayer.Play();
            }
            else if (timerType == TimerType.Rest)
            {
                restSoundPlayer.Play();
            }
            else
            {
                throw new ArgumentException($"Unhandled TimerType: {timerType.ToString()}");
            }
        }
    }
}
