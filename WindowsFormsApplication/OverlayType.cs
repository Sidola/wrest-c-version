﻿namespace Wrest
{
    enum OverlayType
    {
        FULLSCREEN,
        BIG_WINDOWED,
        SMALL_WINDOWED
    }
}
