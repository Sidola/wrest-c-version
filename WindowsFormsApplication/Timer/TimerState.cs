﻿namespace Wrest
{
    internal enum TimerState
    {
        BEFORE_STARTED,
        AFTER_STARTED,

        BEFORE_STOPPED,
        AFTER_STOPPED,

        BEORE_PAUSED,
        AFTER_PAUSED,

        BEFORE_RESUMED,
        AFTER_RESUMED
    }
}
