﻿using System;
using System.Windows.Forms;

namespace Wrest
{
    internal delegate void TimerChangedStateEventHandler(object sender, TimerChangedStateArgs args);
    internal delegate void ActiveTimerChangedEventHandler(object sender, ActiveTimerChangedArgs args);
    internal delegate void ActiveTimerTickEventHandler(object sender, ActiveTimerTickEventArgs args);

    class TimerManager
    {
        private TypedTimer activeTimer;
        private TypedTimer workTimer;
        private TypedTimer restTimer;

        private Timer activeTickTimer = new Timer();

        internal TimerManager()
        {
            workTimer = new TypedTimer(TimerType.Work);
            restTimer = new TypedTimer(TimerType.Rest);
            activeTimer = workTimer;

            activeTickTimer.Interval = 1;
            activeTickTimer.Tick += ActiveTickTimer_Tick;
            activeTickTimer.Start();
        }

        private void ActiveTickTimer_Tick(object sender, EventArgs e)
        {
            if (IsRunning == false)
                return;

            if (activeTimer.RemainingTimeMilli <= 0)
                return;

            OnActiveTimerTick(new ActiveTimerTickEventArgs(activeTimer.TimerType, activeTimer.RemainingTimeMilli));
        }

        #region // ------- Public API ------- //

        /// <summary>
        /// Resets and starts the active timer.
        /// </summary>
        internal void Start()
        {
            OnTimerChangedState(new TimerChangedStateArgs(TimerState.BEFORE_STARTED, activeTimer.TimerType));

            if (activeTimer.IsTimerStarted)
            {
                return;
            }

            activeTimer.Start();
            OnTimerChangedState(new TimerChangedStateArgs(TimerState.AFTER_STARTED, activeTimer.TimerType));
        }

        /// <summary>
        /// Stops and resets the active timer.
        /// </summary>
        internal void Stop()
        {
            OnTimerChangedState(new TimerChangedStateArgs(TimerState.BEFORE_STOPPED, activeTimer.TimerType));

            if (activeTimer.IsTimerStarted == false)
            {
                return;
            }

            activeTimer.StopAndReset();
            OnTimerChangedState(new TimerChangedStateArgs(TimerState.AFTER_STOPPED, activeTimer.TimerType));
        }

        /// <summary>
        /// Resumes the active timer.
        /// </summary>
        internal void Resume()
        {
            if (activeTimer.IsTimerStarted == false)
            {
                return;
            }

            if (activeTimer.IsPaused == false)
            {
                return;
            }

            activeTimer.Resume();
            OnTimerChangedState(new TimerChangedStateArgs(TimerState.AFTER_RESUMED, activeTimer.TimerType));
        }

        /// <summary>
        /// Pauses the active timer.
        /// </summary>
        internal void Pause()
        {
            if (activeTimer.IsTimerStarted == false )
            {
                return;
            }

            if (activeTimer.IsPaused)
            {
                return;
            }

            activeTimer.Pause();
            OnTimerChangedState(new TimerChangedStateArgs(TimerState.AFTER_PAUSED, activeTimer.TimerType));
        }

        /// <summary>
        /// Sets the duration for the given timer.
        /// You cannot use this method while the timer is running.
        /// </summary
        internal void SetTimerDuration(TimerType timerType, long durationMilli)
        {
            if (activeTimer.IsTimerStarted)
            {
                throw new InvalidOperationException("Cannot set a duration while the timer is running.");
            }

            switch (timerType)
            {
                case TimerType.Work:
                    workTimer.Duration = durationMilli;
                    break;
                case TimerType.Rest:
                    restTimer.Duration = durationMilli;
                    break;
                default:
                    throw new ArgumentException("Invalid timerType", timerType.ToString());
            }
        }
    
        /// <summary>
        /// Sets the current active timer.
        /// You cannot use this method while the timer is running.
        /// </summary>
        internal void SetActiveTimer(TimerType timerType)
        {
            if (activeTimer.IsTimerStarted)
            {
                throw new InvalidOperationException("Cannot set a specific active timer while the timer is running.");
            }

            var previousActiveTimerType = activeTimer.TimerType;

            switch (timerType)
            {
                case TimerType.Work:
                    activeTimer = workTimer;
                    break;
                case TimerType.Rest:
                    activeTimer = restTimer;
                    break;
                default:
                    throw new ArgumentException("Invalid timerType", timerType.ToString());
            }

            var timerIsStarted = false;
            OnActiveTimerChanged(new ActiveTimerChangedArgs(
                    timerIsStarted, previousActiveTimerType, timerType));
        }

        /// <summary>
        /// Switches the currently active timer.
        /// Will retain running/paused states from the previous timer.
        /// </summary>
        internal void SwitchActiveTimer()
        {
            var timerStarted = activeTimer.IsTimerStarted;
            var timerPaused = activeTimer.IsPaused;

            activeTimer.StopAndReset();

            var previousTimerType = activeTimer.TimerType;
            switch (activeTimer.TimerType)
            {
                case TimerType.Work:
                    activeTimer = restTimer;
                    break;
                case TimerType.Rest:
                    activeTimer = workTimer;
                    break;
                default:
                    throw new ArgumentException("Invalid timerType", activeTimer.TimerType.ToString());
            }

            if (timerStarted)
            {
                activeTimer.Start();
            }

            if (timerPaused)
            {
                activeTimer.Pause();
            }

            var timerIsRunning = true;
            OnActiveTimerChanged(new ActiveTimerChangedArgs(
                    timerIsRunning, previousTimerType, activeTimer.TimerType));
        }

        internal void IncrementRunningTimerDurationByMilli(long milli)
        {
            if (IsRunning == false)
            {
                throw new InvalidOperationException("Cannot increment the timer if it's not running");
            }

            activeTimer.IncrementCurrentDurationByMilli(milli);
        }

        #endregion


        #region // ------- Public Events ------- //

        /// <summary>
        /// Fires whenever the active timer changes state.
        /// </summary>
        internal event TimerChangedStateEventHandler TimerChangedState;

        internal void OnTimerChangedState(TimerChangedStateArgs eventArgs)
        {
            if (TimerChangedState == null)
                return;

            TimerChangedState(this, eventArgs);
        }

        /// <summary>
        /// Fires whenever the active timer is swapped out.
        /// </summary>
        internal event ActiveTimerChangedEventHandler ActiveTimerChanged;

        internal void OnActiveTimerChanged(ActiveTimerChangedArgs eventArgs)
        {
            if (ActiveTimerChanged == null)
                return;

            ActiveTimerChanged(this, eventArgs);
        }

        /// <summary>
        /// Fires whenever the active timer is running.
        /// </summary>
        internal event ActiveTimerTickEventHandler ActiveTimerTick;

        internal void OnActiveTimerTick(ActiveTimerTickEventArgs args)
        {
            if (ActiveTimerTick == null)
                return;

            ActiveTimerTick(this, args);
        }

        #endregion


        #region // ------- Public Properties ------- //

        internal TimerType ActiveTimerType
        {
            get { return activeTimer.TimerType; }
        }

        internal string ActiveTimerName
        {
            get { return activeTimer.TimerName; }
        }

        internal long RemainingTimeMilli
        {
            get { return activeTimer.RemainingTimeMilli; }
        }

        /// <summary>
        /// Returns true if the current timer is running or is paused.
        /// </summary>
        internal bool IsStarted
        {
            get { return activeTimer.IsTimerStarted; }
        }

        internal bool IsPaused
        {
            get { return activeTimer.IsPaused; }
        }

        /// <summary>
        /// Returns true if the current timer is running and isn't paused.
        /// </summary>
        internal bool IsRunning
        {
            get { return (IsStarted && !IsPaused); }
        }

        internal long ActiveTimerDuration
        {
            get { return activeTimer.Duration; }
        }

        #endregion

    }
}
