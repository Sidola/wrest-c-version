﻿using System.Diagnostics;

namespace Wrest
{
    class TypedTimer
    {
        private TimerType timerType;

        private Stopwatch timer = new Stopwatch();
        private long currentDuration = 0;
        private long duration = 0;
        private bool paused = false;

        internal TypedTimer(TimerType timerType)
        {
            this.timerType = timerType;
        }

        #region // ------- Public API ------- //

        internal void Start()
        {
            currentDuration = duration;

            timer.Reset();
            timer.Start();

            paused = false;
        }
        
        internal void StopAndReset()
        {
            timer.Stop();
            timer.Reset();

            paused = false;
        }

        internal void Resume()
        {
            timer.Start();
            paused = false;
        }

        internal void Pause()
        {
            timer.Stop();
            paused = true;
        }

        internal void IncrementCurrentDurationByMilli(long milli)
        {
            currentDuration += milli;
        }

        #endregion

        #region // ------- Public Properties ------- //

        internal bool IsPaused { get { return paused; } }

        internal bool IsTimerStarted
        {
            get
            {
                return (paused || timer.IsRunning);
            }
        }

        internal string TimerName { get { return timerType.ToString(); } }

        internal long Duration
        {
            get { return duration; }
            set { duration = value; }
        }

        internal long RemainingTimeMilli
        {
            get { return currentDuration - timer.ElapsedMilliseconds; }
        }

        internal TimerType TimerType { get { return timerType; } }

        #endregion

    }
}
