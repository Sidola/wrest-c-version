﻿using Newtonsoft.Json;
using System;
using System.IO;
using Wrest.Util;

namespace Wrest
{
    internal class ConfigManager
    {
        private static Config config;

        private static string configFolderPath;
        private static string configFilePath;

        private ConfigManager()
        {
            throw new InvalidOperationException("Static class, don't instantiate");
        }

        static ConfigManager()
        {
            var userDocumentsFolder = Environment.GetFolderPath(Environment‌​.SpecialFolder.MyDoc‌​uments);

            configFolderPath = Path.Combine(userDocumentsFolder, Properties.Settings.Default.AppName);

            configFilePath = Path.Combine(
                userDocumentsFolder,
                Properties.Settings.Default.AppName,
                Properties.Settings.Default.SettingsFileName);

            if (Directory.Exists(configFolderPath) == false)
            {
                Directory.CreateDirectory(configFolderPath);
            }
        }

        internal static Config ReadFromDisk()
        {
            if (Program.isDebugMode || File.Exists(configFilePath) == false)
            {
                DebugLogger.Log("Program is in debug mode, loading default config");

                config = DefaultConfigInstance.Config;
            }
            else
            {
                var jsonText = File.ReadAllText(configFilePath);
                config = JsonConvert.DeserializeObject<Config>(jsonText);
            }

            return config;
        }

        internal static void WriteToDisk()
        {
            if (Program.isDebugMode)
            {
                DebugLogger.Log("Program is in debug mode, will not write config to disk");
                return;
            }

            JsonSerializer serializer = new JsonSerializer();

            using (StreamWriter sw = new StreamWriter(configFilePath))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Formatting = Formatting.Indented;
                serializer.Serialize(writer, config);
            }
        }

        /// <summary>
        /// Attempts to get a config value if the given condition
        /// is true. If false, returns the given default value.
        /// </summary>
        internal static T GetValueOrDefault<T>(bool condition, T configValue, T defaultValue)
        {
            return (condition) ? configValue : defaultValue;
        }
    }
}
