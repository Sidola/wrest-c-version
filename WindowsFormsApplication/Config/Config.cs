﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace Wrest
{
    internal class Config
    {
        public bool Auto_Start_With_Windows { get; set; }
        public bool Close_Button_Exits { get; set; }
        public int Custom_Extend_Value_1 { get; set; }
        public int Custom_Extend_Value_2 { get; set; }
        public int Custom_Extend_Value_3 { get; set; }
        public bool Disable_Alert_If_Proc_Active { get; set; }
        public string Disable_From_Hours { get; set; }
        public bool Disable_Overlay_If_Proc_Active { get; set; }
        public bool Disable_Sound_During_Hours { get; set; }
        public string Disable_To_Hours { get; set; }

        public string OOT_Proc_CsvList { get; set; }

        [JsonIgnore]
        public List<string> OOT_Proc_List
        {
            get { return OOT_Proc_CsvList.Split(';').ToList(); }
        }

        public string Overlay_Proc_CsvList { get; set; }

        [JsonIgnore]
        public List<string> Overlay_Proc_List
        {
            get { return Overlay_Proc_CsvList.Split(';').ToList(); }
        }

        public OverlayType Overlay_Type { get; set; }
        public bool Play_Sound_Rest_Ends { get; set; }
        public bool Play_Sound_Work_Ends { get; set; }
        public string Rest_Duration_String { get; set; }
        public bool Show_Delay_Options { get; set; }
        public bool Show_Out_Of_Time_Alert { get; set; }
        public bool Show_Overlay_During_Rest { get; set; }
        public bool Use_Custom_Extend_Values { get; set; }
        public bool Use_OOT_Proc_List_For_Overlay { get; set; }
        public string Work_Duration_String { get; set; }
    }
}
