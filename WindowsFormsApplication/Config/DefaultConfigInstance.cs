﻿namespace Wrest
{
    /// <summary>
    /// Default config instance. Used for debug mode and as the default
    /// for whenever the user is missing a config.
    /// </summary>
    internal class DefaultConfigInstance
    {
        internal static Config Config
        {
            get
            {
                return new Config()
                {
                    #region // ------- Hidden ------- //

                    Work_Duration_String = "0:27:0",
                    Rest_Duration_String = "0:3:0",

                    #endregion


                    #region // ------- OOT Alert ------- //

                    Show_Out_Of_Time_Alert = true,
                    Show_Delay_Options = true,
                    Disable_Alert_If_Proc_Active = false,
                    OOT_Proc_CsvList = "",

                    #endregion


                    #region // ------- Overlay ------- //

                    Show_Overlay_During_Rest = true,
                    Disable_Overlay_If_Proc_Active = true,
                    Use_OOT_Proc_List_For_Overlay = true,
                    Overlay_Proc_CsvList = "",

                    #endregion


                    #region // ------- Overlay Type ------- //

                    Overlay_Type = OverlayType.BIG_WINDOWED,

                    #endregion


                    #region // ------- Sound ------- //

                    Play_Sound_Work_Ends = false,
                    Play_Sound_Rest_Ends = false,
                    Disable_Sound_During_Hours = false,
                    Disable_From_Hours = "23:00",
                    Disable_To_Hours = "07:00",

                    #endregion


                    #region // ------- General ------- //

                    Auto_Start_With_Windows = true,
                    Close_Button_Exits = false,
                    Use_Custom_Extend_Values = false,
                    Custom_Extend_Value_1 = 5,
                    Custom_Extend_Value_2 = 15,
                    Custom_Extend_Value_3 = 30,

                    #endregion
                };
            }
        }
    }
}
