﻿namespace Wrest
{
    partial class ConfigView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigView));
            this.TopPanel = new System.Windows.Forms.Panel();
            this.Settings_Grid = new System.Windows.Forms.TableLayoutPanel();
            this.Overlay_Type_Grid = new System.Windows.Forms.TableLayoutPanel();
            this.SmallWindowed_RB = new System.Windows.Forms.RadioButton();
            this.BigWindowed_RB = new System.Windows.Forms.RadioButton();
            this.Fullscreen_RB = new System.Windows.Forms.RadioButton();
            this.Overlay_Type_Label_BG = new System.Windows.Forms.Panel();
            this.Overlay_Type_Label = new System.Windows.Forms.Label();
            this.General_Grid = new System.Windows.Forms.TableLayoutPanel();
            this.Use_Custom_Value_CB = new System.Windows.Forms.CheckBox();
            this.Close_Btn_Exit_CB = new System.Windows.Forms.CheckBox();
            this.Start_With_Windows_CB = new System.Windows.Forms.CheckBox();
            this.General_Label_BG = new System.Windows.Forms.Panel();
            this.General_Label = new System.Windows.Forms.Label();
            this.ExtendValue_FlowPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.Extend_Value_1_TextArea = new System.Windows.Forms.TextBox();
            this.Extend_Value_2_TextArea = new System.Windows.Forms.TextBox();
            this.Extend_Value_3_TextArea = new System.Windows.Forms.TextBox();
            this.Sound_Grid = new System.Windows.Forms.TableLayoutPanel();
            this.Disable_Sounds_CB = new System.Windows.Forms.CheckBox();
            this.Play_Rest_Sound_CB = new System.Windows.Forms.CheckBox();
            this.Play_Work_Sound_CB = new System.Windows.Forms.CheckBox();
            this.Sound_Label_BG = new System.Windows.Forms.Panel();
            this.Sound_Label = new System.Windows.Forms.Label();
            this.From_To_Flexpanel = new System.Windows.Forms.FlowLayoutPanel();
            this.From_Label = new System.Windows.Forms.Label();
            this.From_DP = new System.Windows.Forms.DateTimePicker();
            this.To_Label = new System.Windows.Forms.Label();
            this.To_DP = new System.Windows.Forms.DateTimePicker();
            this.Overlay_Grid = new System.Windows.Forms.TableLayoutPanel();
            this.Overlay_Disable_Proc_TextArea = new System.Windows.Forms.TextBox();
            this.Overlay_Disable_Proc_Label = new System.Windows.Forms.Label();
            this.Use_Same_As_OOT_Process_CB = new System.Windows.Forms.CheckBox();
            this.Disable_Overlay_If_Process_Active_CB = new System.Windows.Forms.CheckBox();
            this.Show_Overlay_During_Rest_CB = new System.Windows.Forms.CheckBox();
            this.Overlay_Label_BG = new System.Windows.Forms.Panel();
            this.Overlay_Label = new System.Windows.Forms.Label();
            this.OOT_Grid = new System.Windows.Forms.TableLayoutPanel();
            this.OOT_Disable_Proc_TextArea = new System.Windows.Forms.TextBox();
            this.OOT_Disable_Proc_Label = new System.Windows.Forms.Label();
            this.OOT_Disable_If_Proc_Active_CB = new System.Windows.Forms.CheckBox();
            this.OOT_Show_Delay_CB = new System.Windows.Forms.CheckBox();
            this.OOT_Show_Alert_CB = new System.Windows.Forms.CheckBox();
            this.OOT_Alert_Label_BG = new System.Windows.Forms.Panel();
            this.OOT_Alert_Label = new System.Windows.Forms.Label();
            this.BottomPanel = new System.Windows.Forms.Panel();
            this.Buttons_Panel = new System.Windows.Forms.Panel();
            this.Save_Btn = new System.Windows.Forms.Button();
            this.Is_This_Honestly_How_One_Makes_A_Space_Between_Buttons_Jesus_Christ = new System.Windows.Forms.Panel();
            this.Close_Btn = new System.Windows.Forms.Button();
            this.Silver_Border = new System.Windows.Forms.Panel();
            this.White_Border = new System.Windows.Forms.Panel();
            this.TopPanel.SuspendLayout();
            this.Settings_Grid.SuspendLayout();
            this.Overlay_Type_Grid.SuspendLayout();
            this.Overlay_Type_Label_BG.SuspendLayout();
            this.General_Grid.SuspendLayout();
            this.General_Label_BG.SuspendLayout();
            this.ExtendValue_FlowPanel.SuspendLayout();
            this.Sound_Grid.SuspendLayout();
            this.Sound_Label_BG.SuspendLayout();
            this.From_To_Flexpanel.SuspendLayout();
            this.Overlay_Grid.SuspendLayout();
            this.Overlay_Label_BG.SuspendLayout();
            this.OOT_Grid.SuspendLayout();
            this.OOT_Alert_Label_BG.SuspendLayout();
            this.BottomPanel.SuspendLayout();
            this.Buttons_Panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // TopPanel
            // 
            this.TopPanel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.TopPanel.Controls.Add(this.Settings_Grid);
            this.TopPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TopPanel.Location = new System.Drawing.Point(0, 0);
            this.TopPanel.Margin = new System.Windows.Forms.Padding(0);
            this.TopPanel.Name = "TopPanel";
            this.TopPanel.Size = new System.Drawing.Size(570, 465);
            this.TopPanel.TabIndex = 0;
            // 
            // Settings_Grid
            // 
            this.Settings_Grid.ColumnCount = 2;
            this.Settings_Grid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.Settings_Grid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.Settings_Grid.Controls.Add(this.Overlay_Type_Grid, 0, 2);
            this.Settings_Grid.Controls.Add(this.General_Grid, 1, 1);
            this.Settings_Grid.Controls.Add(this.Sound_Grid, 1, 0);
            this.Settings_Grid.Controls.Add(this.Overlay_Grid, 0, 1);
            this.Settings_Grid.Controls.Add(this.OOT_Grid, 0, 0);
            this.Settings_Grid.Dock = System.Windows.Forms.DockStyle.Top;
            this.Settings_Grid.Location = new System.Drawing.Point(0, 0);
            this.Settings_Grid.Margin = new System.Windows.Forms.Padding(0);
            this.Settings_Grid.Name = "Settings_Grid";
            this.Settings_Grid.RowCount = 3;
            this.Settings_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.Settings_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.Settings_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.Settings_Grid.Size = new System.Drawing.Size(570, 457);
            this.Settings_Grid.TabIndex = 0;
            // 
            // Overlay_Type_Grid
            // 
            this.Overlay_Type_Grid.AutoSize = true;
            this.Overlay_Type_Grid.ColumnCount = 1;
            this.Overlay_Type_Grid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.Overlay_Type_Grid.Controls.Add(this.SmallWindowed_RB, 0, 6);
            this.Overlay_Type_Grid.Controls.Add(this.BigWindowed_RB, 0, 4);
            this.Overlay_Type_Grid.Controls.Add(this.Fullscreen_RB, 0, 2);
            this.Overlay_Type_Grid.Controls.Add(this.Overlay_Type_Label_BG, 0, 0);
            this.Overlay_Type_Grid.Dock = System.Windows.Forms.DockStyle.Top;
            this.Overlay_Type_Grid.Location = new System.Drawing.Point(10, 356);
            this.Overlay_Type_Grid.Margin = new System.Windows.Forms.Padding(10, 10, 5, 10);
            this.Overlay_Type_Grid.Name = "Overlay_Type_Grid";
            this.Overlay_Type_Grid.RowCount = 7;
            this.Overlay_Type_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.Overlay_Type_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.Overlay_Type_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.Overlay_Type_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.Overlay_Type_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.Overlay_Type_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.Overlay_Type_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.Overlay_Type_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.Overlay_Type_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.Overlay_Type_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.Overlay_Type_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.Overlay_Type_Grid.Size = new System.Drawing.Size(270, 101);
            this.Overlay_Type_Grid.TabIndex = 8;
            // 
            // SmallWindowed_RB
            // 
            this.SmallWindowed_RB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SmallWindowed_RB.Enabled = false;
            this.SmallWindowed_RB.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SmallWindowed_RB.Location = new System.Drawing.Point(0, 78);
            this.SmallWindowed_RB.Margin = new System.Windows.Forms.Padding(0);
            this.SmallWindowed_RB.Name = "SmallWindowed_RB";
            this.SmallWindowed_RB.Padding = new System.Windows.Forms.Padding(7, 0, 0, 0);
            this.SmallWindowed_RB.Size = new System.Drawing.Size(270, 23);
            this.SmallWindowed_RB.TabIndex = 3;
            this.SmallWindowed_RB.TabStop = true;
            this.SmallWindowed_RB.Text = "Small windowed overlay";
            this.SmallWindowed_RB.UseVisualStyleBackColor = true;
            // 
            // BigWindowed_RB
            // 
            this.BigWindowed_RB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BigWindowed_RB.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BigWindowed_RB.Location = new System.Drawing.Point(0, 53);
            this.BigWindowed_RB.Margin = new System.Windows.Forms.Padding(0);
            this.BigWindowed_RB.Name = "BigWindowed_RB";
            this.BigWindowed_RB.Padding = new System.Windows.Forms.Padding(7, 0, 0, 0);
            this.BigWindowed_RB.Size = new System.Drawing.Size(270, 23);
            this.BigWindowed_RB.TabIndex = 2;
            this.BigWindowed_RB.TabStop = true;
            this.BigWindowed_RB.Text = "Big windowed overlay";
            this.BigWindowed_RB.UseVisualStyleBackColor = true;
            // 
            // Fullscreen_RB
            // 
            this.Fullscreen_RB.AutoSize = true;
            this.Fullscreen_RB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Fullscreen_RB.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Fullscreen_RB.Location = new System.Drawing.Point(0, 28);
            this.Fullscreen_RB.Margin = new System.Windows.Forms.Padding(0);
            this.Fullscreen_RB.Name = "Fullscreen_RB";
            this.Fullscreen_RB.Padding = new System.Windows.Forms.Padding(7, 0, 0, 0);
            this.Fullscreen_RB.Size = new System.Drawing.Size(270, 23);
            this.Fullscreen_RB.TabIndex = 1;
            this.Fullscreen_RB.TabStop = true;
            this.Fullscreen_RB.Text = "Fullscreen overlay";
            this.Fullscreen_RB.UseVisualStyleBackColor = true;
            // 
            // Overlay_Type_Label_BG
            // 
            this.Overlay_Type_Label_BG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(218)))), ((int)(((byte)(218)))));
            this.Overlay_Type_Label_BG.Controls.Add(this.Overlay_Type_Label);
            this.Overlay_Type_Label_BG.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.Overlay_Type_Label_BG.Location = new System.Drawing.Point(0, 0);
            this.Overlay_Type_Label_BG.Margin = new System.Windows.Forms.Padding(0);
            this.Overlay_Type_Label_BG.Name = "Overlay_Type_Label_BG";
            this.Overlay_Type_Label_BG.Size = new System.Drawing.Size(270, 23);
            this.Overlay_Type_Label_BG.TabIndex = 0;
            // 
            // Overlay_Type_Label
            // 
            this.Overlay_Type_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Overlay_Type_Label.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Overlay_Type_Label.ForeColor = System.Drawing.Color.Black;
            this.Overlay_Type_Label.Location = new System.Drawing.Point(0, 0);
            this.Overlay_Type_Label.Margin = new System.Windows.Forms.Padding(0);
            this.Overlay_Type_Label.Name = "Overlay_Type_Label";
            this.Overlay_Type_Label.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.Overlay_Type_Label.Size = new System.Drawing.Size(270, 23);
            this.Overlay_Type_Label.TabIndex = 0;
            this.Overlay_Type_Label.Text = "Overlay Type";
            this.Overlay_Type_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // General_Grid
            // 
            this.General_Grid.AutoSize = true;
            this.General_Grid.ColumnCount = 1;
            this.General_Grid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.General_Grid.Controls.Add(this.Use_Custom_Value_CB, 0, 6);
            this.General_Grid.Controls.Add(this.Close_Btn_Exit_CB, 0, 4);
            this.General_Grid.Controls.Add(this.Start_With_Windows_CB, 0, 2);
            this.General_Grid.Controls.Add(this.General_Label_BG, 0, 0);
            this.General_Grid.Controls.Add(this.ExtendValue_FlowPanel, 0, 8);
            this.General_Grid.Dock = System.Windows.Forms.DockStyle.Top;
            this.General_Grid.Location = new System.Drawing.Point(290, 183);
            this.General_Grid.Margin = new System.Windows.Forms.Padding(5, 10, 10, 10);
            this.General_Grid.Name = "General_Grid";
            this.General_Grid.RowCount = 11;
            this.General_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.General_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.General_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.General_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.General_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.General_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.General_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.General_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 4F));
            this.General_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.General_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.General_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.General_Grid.Size = new System.Drawing.Size(270, 153);
            this.General_Grid.TabIndex = 7;
            // 
            // Use_Custom_Value_CB
            // 
            this.Use_Custom_Value_CB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Use_Custom_Value_CB.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Use_Custom_Value_CB.Location = new System.Drawing.Point(0, 78);
            this.Use_Custom_Value_CB.Margin = new System.Windows.Forms.Padding(0);
            this.Use_Custom_Value_CB.Name = "Use_Custom_Value_CB";
            this.Use_Custom_Value_CB.Padding = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.Use_Custom_Value_CB.Size = new System.Drawing.Size(270, 23);
            this.Use_Custom_Value_CB.TabIndex = 3;
            this.Use_Custom_Value_CB.Text = "Use custom extend values (minutes)";
            this.Use_Custom_Value_CB.UseVisualStyleBackColor = true;
            this.Use_Custom_Value_CB.CheckedChanged += new System.EventHandler(this.Use_Custom_Value_CB_CheckedChanged);
            // 
            // Close_Btn_Exit_CB
            // 
            this.Close_Btn_Exit_CB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Close_Btn_Exit_CB.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Close_Btn_Exit_CB.Location = new System.Drawing.Point(0, 53);
            this.Close_Btn_Exit_CB.Margin = new System.Windows.Forms.Padding(0);
            this.Close_Btn_Exit_CB.Name = "Close_Btn_Exit_CB";
            this.Close_Btn_Exit_CB.Padding = new System.Windows.Forms.Padding(7, 0, 0, 0);
            this.Close_Btn_Exit_CB.Size = new System.Drawing.Size(270, 23);
            this.Close_Btn_Exit_CB.TabIndex = 2;
            this.Close_Btn_Exit_CB.Text = "Close button exits";
            this.Close_Btn_Exit_CB.UseVisualStyleBackColor = true;
            // 
            // Start_With_Windows_CB
            // 
            this.Start_With_Windows_CB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Start_With_Windows_CB.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Start_With_Windows_CB.Location = new System.Drawing.Point(0, 28);
            this.Start_With_Windows_CB.Margin = new System.Windows.Forms.Padding(0);
            this.Start_With_Windows_CB.Name = "Start_With_Windows_CB";
            this.Start_With_Windows_CB.Padding = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.Start_With_Windows_CB.Size = new System.Drawing.Size(270, 23);
            this.Start_With_Windows_CB.TabIndex = 1;
            this.Start_With_Windows_CB.Text = "Auto start with Windows";
            this.Start_With_Windows_CB.UseVisualStyleBackColor = true;
            // 
            // General_Label_BG
            // 
            this.General_Label_BG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(218)))), ((int)(((byte)(218)))));
            this.General_Label_BG.Controls.Add(this.General_Label);
            this.General_Label_BG.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.General_Label_BG.Location = new System.Drawing.Point(0, 0);
            this.General_Label_BG.Margin = new System.Windows.Forms.Padding(0);
            this.General_Label_BG.Name = "General_Label_BG";
            this.General_Label_BG.Size = new System.Drawing.Size(270, 23);
            this.General_Label_BG.TabIndex = 0;
            // 
            // General_Label
            // 
            this.General_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.General_Label.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.General_Label.ForeColor = System.Drawing.Color.Black;
            this.General_Label.Location = new System.Drawing.Point(0, 0);
            this.General_Label.Margin = new System.Windows.Forms.Padding(0);
            this.General_Label.Name = "General_Label";
            this.General_Label.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.General_Label.Size = new System.Drawing.Size(270, 23);
            this.General_Label.TabIndex = 0;
            this.General_Label.Text = "General";
            this.General_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ExtendValue_FlowPanel
            // 
            this.ExtendValue_FlowPanel.Controls.Add(this.Extend_Value_1_TextArea);
            this.ExtendValue_FlowPanel.Controls.Add(this.Extend_Value_2_TextArea);
            this.ExtendValue_FlowPanel.Controls.Add(this.Extend_Value_3_TextArea);
            this.ExtendValue_FlowPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ExtendValue_FlowPanel.Location = new System.Drawing.Point(7, 105);
            this.ExtendValue_FlowPanel.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.ExtendValue_FlowPanel.Name = "ExtendValue_FlowPanel";
            this.ExtendValue_FlowPanel.Size = new System.Drawing.Size(256, 23);
            this.ExtendValue_FlowPanel.TabIndex = 4;
            // 
            // Extend_Value_1_TextArea
            // 
            this.Extend_Value_1_TextArea.Dock = System.Windows.Forms.DockStyle.Left;
            this.Extend_Value_1_TextArea.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Extend_Value_1_TextArea.Location = new System.Drawing.Point(0, 0);
            this.Extend_Value_1_TextArea.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.Extend_Value_1_TextArea.Name = "Extend_Value_1_TextArea";
            this.Extend_Value_1_TextArea.Size = new System.Drawing.Size(76, 23);
            this.Extend_Value_1_TextArea.TabIndex = 0;
            this.Extend_Value_1_TextArea.Text = "5";
            this.Extend_Value_1_TextArea.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Extend_Value_2_TextArea
            // 
            this.Extend_Value_2_TextArea.Dock = System.Windows.Forms.DockStyle.Left;
            this.Extend_Value_2_TextArea.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Extend_Value_2_TextArea.Location = new System.Drawing.Point(86, 0);
            this.Extend_Value_2_TextArea.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.Extend_Value_2_TextArea.Name = "Extend_Value_2_TextArea";
            this.Extend_Value_2_TextArea.Size = new System.Drawing.Size(76, 23);
            this.Extend_Value_2_TextArea.TabIndex = 1;
            this.Extend_Value_2_TextArea.Text = "15";
            this.Extend_Value_2_TextArea.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Extend_Value_3_TextArea
            // 
            this.Extend_Value_3_TextArea.Dock = System.Windows.Forms.DockStyle.Left;
            this.Extend_Value_3_TextArea.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Extend_Value_3_TextArea.Location = new System.Drawing.Point(172, 0);
            this.Extend_Value_3_TextArea.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.Extend_Value_3_TextArea.Name = "Extend_Value_3_TextArea";
            this.Extend_Value_3_TextArea.Size = new System.Drawing.Size(76, 23);
            this.Extend_Value_3_TextArea.TabIndex = 2;
            this.Extend_Value_3_TextArea.Text = "30";
            this.Extend_Value_3_TextArea.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Sound_Grid
            // 
            this.Sound_Grid.AutoSize = true;
            this.Sound_Grid.ColumnCount = 1;
            this.Sound_Grid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.Sound_Grid.Controls.Add(this.Disable_Sounds_CB, 0, 6);
            this.Sound_Grid.Controls.Add(this.Play_Rest_Sound_CB, 0, 4);
            this.Sound_Grid.Controls.Add(this.Play_Work_Sound_CB, 0, 2);
            this.Sound_Grid.Controls.Add(this.Sound_Label_BG, 0, 0);
            this.Sound_Grid.Controls.Add(this.From_To_Flexpanel, 0, 8);
            this.Sound_Grid.Dock = System.Windows.Forms.DockStyle.Top;
            this.Sound_Grid.Location = new System.Drawing.Point(290, 10);
            this.Sound_Grid.Margin = new System.Windows.Forms.Padding(5, 10, 10, 10);
            this.Sound_Grid.Name = "Sound_Grid";
            this.Sound_Grid.RowCount = 11;
            this.Sound_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.Sound_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.Sound_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.Sound_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.Sound_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.Sound_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.Sound_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.Sound_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 4F));
            this.Sound_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.Sound_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.Sound_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.Sound_Grid.Size = new System.Drawing.Size(270, 153);
            this.Sound_Grid.TabIndex = 6;
            // 
            // Disable_Sounds_CB
            // 
            this.Disable_Sounds_CB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Disable_Sounds_CB.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Disable_Sounds_CB.Location = new System.Drawing.Point(0, 78);
            this.Disable_Sounds_CB.Margin = new System.Windows.Forms.Padding(0);
            this.Disable_Sounds_CB.Name = "Disable_Sounds_CB";
            this.Disable_Sounds_CB.Padding = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.Disable_Sounds_CB.Size = new System.Drawing.Size(270, 23);
            this.Disable_Sounds_CB.TabIndex = 3;
            this.Disable_Sounds_CB.Text = "Disable sounds during certain hours";
            this.Disable_Sounds_CB.UseVisualStyleBackColor = true;
            this.Disable_Sounds_CB.CheckedChanged += new System.EventHandler(this.Disable_Sounds_CB_CheckedChanged);
            // 
            // Play_Rest_Sound_CB
            // 
            this.Play_Rest_Sound_CB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Play_Rest_Sound_CB.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Play_Rest_Sound_CB.Location = new System.Drawing.Point(0, 53);
            this.Play_Rest_Sound_CB.Margin = new System.Windows.Forms.Padding(0);
            this.Play_Rest_Sound_CB.Name = "Play_Rest_Sound_CB";
            this.Play_Rest_Sound_CB.Padding = new System.Windows.Forms.Padding(7, 0, 0, 0);
            this.Play_Rest_Sound_CB.Size = new System.Drawing.Size(270, 23);
            this.Play_Rest_Sound_CB.TabIndex = 2;
            this.Play_Rest_Sound_CB.Text = "Play sound when rest timer ends";
            this.Play_Rest_Sound_CB.UseVisualStyleBackColor = true;
            // 
            // Play_Work_Sound_CB
            // 
            this.Play_Work_Sound_CB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Play_Work_Sound_CB.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Play_Work_Sound_CB.Location = new System.Drawing.Point(0, 28);
            this.Play_Work_Sound_CB.Margin = new System.Windows.Forms.Padding(0);
            this.Play_Work_Sound_CB.Name = "Play_Work_Sound_CB";
            this.Play_Work_Sound_CB.Padding = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.Play_Work_Sound_CB.Size = new System.Drawing.Size(270, 23);
            this.Play_Work_Sound_CB.TabIndex = 1;
            this.Play_Work_Sound_CB.Text = "Play sound when work timer ends";
            this.Play_Work_Sound_CB.UseVisualStyleBackColor = true;
            // 
            // Sound_Label_BG
            // 
            this.Sound_Label_BG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(218)))), ((int)(((byte)(218)))));
            this.Sound_Label_BG.Controls.Add(this.Sound_Label);
            this.Sound_Label_BG.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.Sound_Label_BG.Location = new System.Drawing.Point(0, 0);
            this.Sound_Label_BG.Margin = new System.Windows.Forms.Padding(0);
            this.Sound_Label_BG.Name = "Sound_Label_BG";
            this.Sound_Label_BG.Size = new System.Drawing.Size(270, 23);
            this.Sound_Label_BG.TabIndex = 0;
            // 
            // Sound_Label
            // 
            this.Sound_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Sound_Label.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sound_Label.ForeColor = System.Drawing.Color.Black;
            this.Sound_Label.Location = new System.Drawing.Point(0, 0);
            this.Sound_Label.Margin = new System.Windows.Forms.Padding(0);
            this.Sound_Label.Name = "Sound_Label";
            this.Sound_Label.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.Sound_Label.Size = new System.Drawing.Size(270, 23);
            this.Sound_Label.TabIndex = 0;
            this.Sound_Label.Text = "Sound";
            this.Sound_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // From_To_Flexpanel
            // 
            this.From_To_Flexpanel.Controls.Add(this.From_Label);
            this.From_To_Flexpanel.Controls.Add(this.From_DP);
            this.From_To_Flexpanel.Controls.Add(this.To_Label);
            this.From_To_Flexpanel.Controls.Add(this.To_DP);
            this.From_To_Flexpanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.From_To_Flexpanel.Location = new System.Drawing.Point(0, 105);
            this.From_To_Flexpanel.Margin = new System.Windows.Forms.Padding(0);
            this.From_To_Flexpanel.Name = "From_To_Flexpanel";
            this.From_To_Flexpanel.Size = new System.Drawing.Size(270, 23);
            this.From_To_Flexpanel.TabIndex = 4;
            // 
            // From_Label
            // 
            this.From_Label.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.From_Label.Location = new System.Drawing.Point(7, 0);
            this.From_Label.Margin = new System.Windows.Forms.Padding(7, 0, 0, 0);
            this.From_Label.Name = "From_Label";
            this.From_Label.Size = new System.Drawing.Size(40, 23);
            this.From_Label.TabIndex = 0;
            this.From_Label.Text = "From:";
            this.From_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // From_DP
            // 
            this.From_DP.CustomFormat = " HH:mm";
            this.From_DP.Dock = System.Windows.Forms.DockStyle.Left;
            this.From_DP.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.From_DP.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.From_DP.Location = new System.Drawing.Point(47, 0);
            this.From_DP.Margin = new System.Windows.Forms.Padding(0);
            this.From_DP.Name = "From_DP";
            this.From_DP.ShowUpDown = true;
            this.From_DP.Size = new System.Drawing.Size(80, 23);
            this.From_DP.TabIndex = 1;
            // 
            // To_Label
            // 
            this.To_Label.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.To_Label.Location = new System.Drawing.Point(134, 0);
            this.To_Label.Margin = new System.Windows.Forms.Padding(7, 0, 0, 0);
            this.To_Label.Name = "To_Label";
            this.To_Label.Size = new System.Drawing.Size(25, 23);
            this.To_Label.TabIndex = 2;
            this.To_Label.Text = "To:";
            this.To_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // To_DP
            // 
            this.To_DP.CustomFormat = " HH:mm";
            this.To_DP.Dock = System.Windows.Forms.DockStyle.Left;
            this.To_DP.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.To_DP.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.To_DP.Location = new System.Drawing.Point(159, 0);
            this.To_DP.Margin = new System.Windows.Forms.Padding(0);
            this.To_DP.Name = "To_DP";
            this.To_DP.ShowUpDown = true;
            this.To_DP.Size = new System.Drawing.Size(80, 23);
            this.To_DP.TabIndex = 3;
            // 
            // Overlay_Grid
            // 
            this.Overlay_Grid.AutoSize = true;
            this.Overlay_Grid.ColumnCount = 1;
            this.Overlay_Grid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.Overlay_Grid.Controls.Add(this.Overlay_Disable_Proc_TextArea, 0, 10);
            this.Overlay_Grid.Controls.Add(this.Overlay_Disable_Proc_Label, 0, 8);
            this.Overlay_Grid.Controls.Add(this.Use_Same_As_OOT_Process_CB, 0, 6);
            this.Overlay_Grid.Controls.Add(this.Disable_Overlay_If_Process_Active_CB, 0, 4);
            this.Overlay_Grid.Controls.Add(this.Show_Overlay_During_Rest_CB, 0, 2);
            this.Overlay_Grid.Controls.Add(this.Overlay_Label_BG, 0, 0);
            this.Overlay_Grid.Dock = System.Windows.Forms.DockStyle.Top;
            this.Overlay_Grid.Location = new System.Drawing.Point(10, 183);
            this.Overlay_Grid.Margin = new System.Windows.Forms.Padding(10, 10, 5, 10);
            this.Overlay_Grid.Name = "Overlay_Grid";
            this.Overlay_Grid.RowCount = 11;
            this.Overlay_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.Overlay_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.Overlay_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.Overlay_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.Overlay_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.Overlay_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.Overlay_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.Overlay_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.Overlay_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.Overlay_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.Overlay_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.Overlay_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.Overlay_Grid.Size = new System.Drawing.Size(270, 151);
            this.Overlay_Grid.TabIndex = 2;
            // 
            // Overlay_Disable_Proc_TextArea
            // 
            this.Overlay_Disable_Proc_TextArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Overlay_Disable_Proc_TextArea.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Overlay_Disable_Proc_TextArea.Location = new System.Drawing.Point(7, 128);
            this.Overlay_Disable_Proc_TextArea.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.Overlay_Disable_Proc_TextArea.Name = "Overlay_Disable_Proc_TextArea";
            this.Overlay_Disable_Proc_TextArea.Size = new System.Drawing.Size(256, 23);
            this.Overlay_Disable_Proc_TextArea.TabIndex = 5;
            // 
            // Overlay_Disable_Proc_Label
            // 
            this.Overlay_Disable_Proc_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Overlay_Disable_Proc_Label.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Overlay_Disable_Proc_Label.ForeColor = System.Drawing.Color.Black;
            this.Overlay_Disable_Proc_Label.Location = new System.Drawing.Point(0, 103);
            this.Overlay_Disable_Proc_Label.Margin = new System.Windows.Forms.Padding(0);
            this.Overlay_Disable_Proc_Label.Name = "Overlay_Disable_Proc_Label";
            this.Overlay_Disable_Proc_Label.Padding = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.Overlay_Disable_Proc_Label.Size = new System.Drawing.Size(270, 23);
            this.Overlay_Disable_Proc_Label.TabIndex = 4;
            this.Overlay_Disable_Proc_Label.Text = "Separate each process with \";\"";
            this.Overlay_Disable_Proc_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Use_Same_As_OOT_Process_CB
            // 
            this.Use_Same_As_OOT_Process_CB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Use_Same_As_OOT_Process_CB.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Use_Same_As_OOT_Process_CB.Location = new System.Drawing.Point(0, 78);
            this.Use_Same_As_OOT_Process_CB.Margin = new System.Windows.Forms.Padding(0);
            this.Use_Same_As_OOT_Process_CB.Name = "Use_Same_As_OOT_Process_CB";
            this.Use_Same_As_OOT_Process_CB.Padding = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.Use_Same_As_OOT_Process_CB.Size = new System.Drawing.Size(270, 23);
            this.Use_Same_As_OOT_Process_CB.TabIndex = 3;
            this.Use_Same_As_OOT_Process_CB.Text = "Use same processes as out-of-time alert";
            this.Use_Same_As_OOT_Process_CB.UseVisualStyleBackColor = true;
            this.Use_Same_As_OOT_Process_CB.CheckedChanged += new System.EventHandler(this.Use_Same_As_OOT_Process_CB_CheckedChanged);
            // 
            // Disable_Overlay_If_Process_Active_CB
            // 
            this.Disable_Overlay_If_Process_Active_CB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Disable_Overlay_If_Process_Active_CB.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Disable_Overlay_If_Process_Active_CB.Location = new System.Drawing.Point(0, 53);
            this.Disable_Overlay_If_Process_Active_CB.Margin = new System.Windows.Forms.Padding(0);
            this.Disable_Overlay_If_Process_Active_CB.Name = "Disable_Overlay_If_Process_Active_CB";
            this.Disable_Overlay_If_Process_Active_CB.Padding = new System.Windows.Forms.Padding(7, 0, 0, 0);
            this.Disable_Overlay_If_Process_Active_CB.Size = new System.Drawing.Size(270, 23);
            this.Disable_Overlay_If_Process_Active_CB.TabIndex = 2;
            this.Disable_Overlay_If_Process_Active_CB.Text = "Disable overlay if certain processes are active";
            this.Disable_Overlay_If_Process_Active_CB.UseVisualStyleBackColor = true;
            this.Disable_Overlay_If_Process_Active_CB.CheckedChanged += new System.EventHandler(this.Disable_OOT_If_Process_Active_CB_CheckedChanged);
            // 
            // Show_Overlay_During_Rest_CB
            // 
            this.Show_Overlay_During_Rest_CB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Show_Overlay_During_Rest_CB.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Show_Overlay_During_Rest_CB.Location = new System.Drawing.Point(0, 28);
            this.Show_Overlay_During_Rest_CB.Margin = new System.Windows.Forms.Padding(0);
            this.Show_Overlay_During_Rest_CB.Name = "Show_Overlay_During_Rest_CB";
            this.Show_Overlay_During_Rest_CB.Padding = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.Show_Overlay_During_Rest_CB.Size = new System.Drawing.Size(270, 23);
            this.Show_Overlay_During_Rest_CB.TabIndex = 1;
            this.Show_Overlay_During_Rest_CB.Text = "Show overlay during rest";
            this.Show_Overlay_During_Rest_CB.UseVisualStyleBackColor = true;
            // 
            // Overlay_Label_BG
            // 
            this.Overlay_Label_BG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(218)))), ((int)(((byte)(218)))));
            this.Overlay_Label_BG.Controls.Add(this.Overlay_Label);
            this.Overlay_Label_BG.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.Overlay_Label_BG.Location = new System.Drawing.Point(0, 0);
            this.Overlay_Label_BG.Margin = new System.Windows.Forms.Padding(0);
            this.Overlay_Label_BG.Name = "Overlay_Label_BG";
            this.Overlay_Label_BG.Size = new System.Drawing.Size(270, 23);
            this.Overlay_Label_BG.TabIndex = 0;
            // 
            // Overlay_Label
            // 
            this.Overlay_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Overlay_Label.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Overlay_Label.ForeColor = System.Drawing.Color.Black;
            this.Overlay_Label.Location = new System.Drawing.Point(0, 0);
            this.Overlay_Label.Margin = new System.Windows.Forms.Padding(0);
            this.Overlay_Label.Name = "Overlay_Label";
            this.Overlay_Label.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.Overlay_Label.Size = new System.Drawing.Size(270, 23);
            this.Overlay_Label.TabIndex = 0;
            this.Overlay_Label.Text = "Overlay";
            this.Overlay_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // OOT_Grid
            // 
            this.OOT_Grid.AutoSize = true;
            this.OOT_Grid.ColumnCount = 1;
            this.OOT_Grid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.OOT_Grid.Controls.Add(this.OOT_Disable_Proc_TextArea, 0, 10);
            this.OOT_Grid.Controls.Add(this.OOT_Disable_Proc_Label, 0, 8);
            this.OOT_Grid.Controls.Add(this.OOT_Disable_If_Proc_Active_CB, 0, 6);
            this.OOT_Grid.Controls.Add(this.OOT_Show_Delay_CB, 0, 4);
            this.OOT_Grid.Controls.Add(this.OOT_Show_Alert_CB, 0, 2);
            this.OOT_Grid.Controls.Add(this.OOT_Alert_Label_BG, 0, 0);
            this.OOT_Grid.Dock = System.Windows.Forms.DockStyle.Top;
            this.OOT_Grid.Location = new System.Drawing.Point(10, 10);
            this.OOT_Grid.Margin = new System.Windows.Forms.Padding(10, 10, 5, 10);
            this.OOT_Grid.Name = "OOT_Grid";
            this.OOT_Grid.RowCount = 11;
            this.OOT_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.OOT_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.OOT_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.OOT_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.OOT_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.OOT_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.OOT_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.OOT_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.OOT_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.OOT_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.OOT_Grid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.OOT_Grid.Size = new System.Drawing.Size(270, 151);
            this.OOT_Grid.TabIndex = 1;
            // 
            // OOT_Disable_Proc_TextArea
            // 
            this.OOT_Disable_Proc_TextArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OOT_Disable_Proc_TextArea.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OOT_Disable_Proc_TextArea.Location = new System.Drawing.Point(7, 128);
            this.OOT_Disable_Proc_TextArea.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.OOT_Disable_Proc_TextArea.Name = "OOT_Disable_Proc_TextArea";
            this.OOT_Disable_Proc_TextArea.Size = new System.Drawing.Size(256, 23);
            this.OOT_Disable_Proc_TextArea.TabIndex = 5;
            // 
            // OOT_Disable_Proc_Label
            // 
            this.OOT_Disable_Proc_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OOT_Disable_Proc_Label.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OOT_Disable_Proc_Label.ForeColor = System.Drawing.Color.Black;
            this.OOT_Disable_Proc_Label.Location = new System.Drawing.Point(0, 103);
            this.OOT_Disable_Proc_Label.Margin = new System.Windows.Forms.Padding(0);
            this.OOT_Disable_Proc_Label.Name = "OOT_Disable_Proc_Label";
            this.OOT_Disable_Proc_Label.Padding = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.OOT_Disable_Proc_Label.Size = new System.Drawing.Size(270, 23);
            this.OOT_Disable_Proc_Label.TabIndex = 4;
            this.OOT_Disable_Proc_Label.Text = "Separate each process with \";\"";
            this.OOT_Disable_Proc_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // OOT_Disable_If_Proc_Active_CB
            // 
            this.OOT_Disable_If_Proc_Active_CB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OOT_Disable_If_Proc_Active_CB.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OOT_Disable_If_Proc_Active_CB.Location = new System.Drawing.Point(0, 78);
            this.OOT_Disable_If_Proc_Active_CB.Margin = new System.Windows.Forms.Padding(0);
            this.OOT_Disable_If_Proc_Active_CB.Name = "OOT_Disable_If_Proc_Active_CB";
            this.OOT_Disable_If_Proc_Active_CB.Padding = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.OOT_Disable_If_Proc_Active_CB.Size = new System.Drawing.Size(270, 23);
            this.OOT_Disable_If_Proc_Active_CB.TabIndex = 3;
            this.OOT_Disable_If_Proc_Active_CB.Text = "Disable alert if certain processes are active";
            this.OOT_Disable_If_Proc_Active_CB.UseVisualStyleBackColor = true;
            this.OOT_Disable_If_Proc_Active_CB.CheckedChanged += new System.EventHandler(this.OOT_Disable_If_Proc_Active_CB_CheckedChanged);
            // 
            // OOT_Show_Delay_CB
            // 
            this.OOT_Show_Delay_CB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OOT_Show_Delay_CB.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OOT_Show_Delay_CB.Location = new System.Drawing.Point(0, 53);
            this.OOT_Show_Delay_CB.Margin = new System.Windows.Forms.Padding(0);
            this.OOT_Show_Delay_CB.Name = "OOT_Show_Delay_CB";
            this.OOT_Show_Delay_CB.Padding = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.OOT_Show_Delay_CB.Size = new System.Drawing.Size(270, 23);
            this.OOT_Show_Delay_CB.TabIndex = 2;
            this.OOT_Show_Delay_CB.Text = "Show delay options";
            this.OOT_Show_Delay_CB.UseVisualStyleBackColor = true;
            // 
            // OOT_Show_Alert_CB
            // 
            this.OOT_Show_Alert_CB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OOT_Show_Alert_CB.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OOT_Show_Alert_CB.Location = new System.Drawing.Point(0, 28);
            this.OOT_Show_Alert_CB.Margin = new System.Windows.Forms.Padding(0);
            this.OOT_Show_Alert_CB.Name = "OOT_Show_Alert_CB";
            this.OOT_Show_Alert_CB.Padding = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.OOT_Show_Alert_CB.Size = new System.Drawing.Size(270, 23);
            this.OOT_Show_Alert_CB.TabIndex = 1;
            this.OOT_Show_Alert_CB.Text = "Show out-of-time alert";
            this.OOT_Show_Alert_CB.UseVisualStyleBackColor = true;
            // 
            // OOT_Alert_Label_BG
            // 
            this.OOT_Alert_Label_BG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(218)))), ((int)(((byte)(218)))));
            this.OOT_Alert_Label_BG.Controls.Add(this.OOT_Alert_Label);
            this.OOT_Alert_Label_BG.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.OOT_Alert_Label_BG.Location = new System.Drawing.Point(0, 0);
            this.OOT_Alert_Label_BG.Margin = new System.Windows.Forms.Padding(0);
            this.OOT_Alert_Label_BG.Name = "OOT_Alert_Label_BG";
            this.OOT_Alert_Label_BG.Size = new System.Drawing.Size(270, 23);
            this.OOT_Alert_Label_BG.TabIndex = 0;
            // 
            // OOT_Alert_Label
            // 
            this.OOT_Alert_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OOT_Alert_Label.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OOT_Alert_Label.ForeColor = System.Drawing.Color.Black;
            this.OOT_Alert_Label.Location = new System.Drawing.Point(0, 0);
            this.OOT_Alert_Label.Margin = new System.Windows.Forms.Padding(0);
            this.OOT_Alert_Label.Name = "OOT_Alert_Label";
            this.OOT_Alert_Label.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.OOT_Alert_Label.Size = new System.Drawing.Size(270, 23);
            this.OOT_Alert_Label.TabIndex = 0;
            this.OOT_Alert_Label.Text = "Out-of-time alert";
            this.OOT_Alert_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // BottomPanel
            // 
            this.BottomPanel.BackColor = System.Drawing.SystemColors.Control;
            this.BottomPanel.Controls.Add(this.Buttons_Panel);
            this.BottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomPanel.Location = new System.Drawing.Point(0, 467);
            this.BottomPanel.Margin = new System.Windows.Forms.Padding(0);
            this.BottomPanel.Name = "BottomPanel";
            this.BottomPanel.Size = new System.Drawing.Size(570, 50);
            this.BottomPanel.TabIndex = 1;
            // 
            // Buttons_Panel
            // 
            this.Buttons_Panel.AutoSize = true;
            this.Buttons_Panel.Controls.Add(this.Save_Btn);
            this.Buttons_Panel.Controls.Add(this.Is_This_Honestly_How_One_Makes_A_Space_Between_Buttons_Jesus_Christ);
            this.Buttons_Panel.Controls.Add(this.Close_Btn);
            this.Buttons_Panel.Dock = System.Windows.Forms.DockStyle.Right;
            this.Buttons_Panel.Location = new System.Drawing.Point(380, 0);
            this.Buttons_Panel.Margin = new System.Windows.Forms.Padding(0);
            this.Buttons_Panel.Name = "Buttons_Panel";
            this.Buttons_Panel.Padding = new System.Windows.Forms.Padding(10);
            this.Buttons_Panel.Size = new System.Drawing.Size(190, 50);
            this.Buttons_Panel.TabIndex = 1;
            // 
            // Save_Btn
            // 
            this.Save_Btn.Dock = System.Windows.Forms.DockStyle.Right;
            this.Save_Btn.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Save_Btn.Location = new System.Drawing.Point(10, 10);
            this.Save_Btn.Margin = new System.Windows.Forms.Padding(0);
            this.Save_Btn.Name = "Save_Btn";
            this.Save_Btn.Size = new System.Drawing.Size(80, 30);
            this.Save_Btn.TabIndex = 1;
            this.Save_Btn.Text = "Save";
            this.Save_Btn.UseVisualStyleBackColor = true;
            this.Save_Btn.Click += new System.EventHandler(this.Save_Btn_Click);
            // 
            // Is_This_Honestly_How_One_Makes_A_Space_Between_Buttons_Jesus_Christ
            // 
            this.Is_This_Honestly_How_One_Makes_A_Space_Between_Buttons_Jesus_Christ.Dock = System.Windows.Forms.DockStyle.Right;
            this.Is_This_Honestly_How_One_Makes_A_Space_Between_Buttons_Jesus_Christ.Location = new System.Drawing.Point(90, 10);
            this.Is_This_Honestly_How_One_Makes_A_Space_Between_Buttons_Jesus_Christ.Name = "Is_This_Honestly_How_One_Makes_A_Space_Between_Buttons_Jesus_Christ";
            this.Is_This_Honestly_How_One_Makes_A_Space_Between_Buttons_Jesus_Christ.Size = new System.Drawing.Size(10, 30);
            this.Is_This_Honestly_How_One_Makes_A_Space_Between_Buttons_Jesus_Christ.TabIndex = 2;
            // 
            // Close_Btn
            // 
            this.Close_Btn.Dock = System.Windows.Forms.DockStyle.Right;
            this.Close_Btn.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Close_Btn.Location = new System.Drawing.Point(100, 10);
            this.Close_Btn.Margin = new System.Windows.Forms.Padding(0);
            this.Close_Btn.Name = "Close_Btn";
            this.Close_Btn.Size = new System.Drawing.Size(80, 30);
            this.Close_Btn.TabIndex = 0;
            this.Close_Btn.Text = "Close";
            this.Close_Btn.UseVisualStyleBackColor = true;
            this.Close_Btn.Click += new System.EventHandler(this.Close_Btn_Click);
            // 
            // Silver_Border
            // 
            this.Silver_Border.BackColor = System.Drawing.Color.Silver;
            this.Silver_Border.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Silver_Border.Location = new System.Drawing.Point(0, 465);
            this.Silver_Border.Name = "Silver_Border";
            this.Silver_Border.Size = new System.Drawing.Size(570, 1);
            this.Silver_Border.TabIndex = 0;
            // 
            // White_Border
            // 
            this.White_Border.BackColor = System.Drawing.Color.White;
            this.White_Border.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.White_Border.Location = new System.Drawing.Point(0, 466);
            this.White_Border.Name = "White_Border";
            this.White_Border.Size = new System.Drawing.Size(570, 1);
            this.White_Border.TabIndex = 1;
            // 
            // ConfigView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(570, 517);
            this.Controls.Add(this.TopPanel);
            this.Controls.Add(this.Silver_Border);
            this.Controls.Add(this.White_Border);
            this.Controls.Add(this.BottomPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfigView";
            this.ShowInTaskbar = false;
            this.Text = "ConfigView";
            this.TopPanel.ResumeLayout(false);
            this.Settings_Grid.ResumeLayout(false);
            this.Settings_Grid.PerformLayout();
            this.Overlay_Type_Grid.ResumeLayout(false);
            this.Overlay_Type_Grid.PerformLayout();
            this.Overlay_Type_Label_BG.ResumeLayout(false);
            this.General_Grid.ResumeLayout(false);
            this.General_Label_BG.ResumeLayout(false);
            this.ExtendValue_FlowPanel.ResumeLayout(false);
            this.ExtendValue_FlowPanel.PerformLayout();
            this.Sound_Grid.ResumeLayout(false);
            this.Sound_Label_BG.ResumeLayout(false);
            this.From_To_Flexpanel.ResumeLayout(false);
            this.Overlay_Grid.ResumeLayout(false);
            this.Overlay_Grid.PerformLayout();
            this.Overlay_Label_BG.ResumeLayout(false);
            this.OOT_Grid.ResumeLayout(false);
            this.OOT_Grid.PerformLayout();
            this.OOT_Alert_Label_BG.ResumeLayout(false);
            this.BottomPanel.ResumeLayout(false);
            this.BottomPanel.PerformLayout();
            this.Buttons_Panel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel TopPanel;
        private System.Windows.Forms.Panel BottomPanel;
        private System.Windows.Forms.Panel Silver_Border;
        private System.Windows.Forms.Panel White_Border;
        private System.Windows.Forms.TableLayoutPanel Settings_Grid;
        private System.Windows.Forms.Panel OOT_Alert_Label_BG;
        private System.Windows.Forms.Label OOT_Alert_Label;
        private System.Windows.Forms.TableLayoutPanel OOT_Grid;
        private System.Windows.Forms.CheckBox OOT_Show_Alert_CB;
        private System.Windows.Forms.CheckBox OOT_Show_Delay_CB;
        private System.Windows.Forms.CheckBox OOT_Disable_If_Proc_Active_CB;
        private System.Windows.Forms.Label OOT_Disable_Proc_Label;
        private System.Windows.Forms.TextBox OOT_Disable_Proc_TextArea;
        private System.Windows.Forms.TableLayoutPanel Overlay_Grid;
        private System.Windows.Forms.TextBox Overlay_Disable_Proc_TextArea;
        private System.Windows.Forms.Label Overlay_Disable_Proc_Label;
        private System.Windows.Forms.CheckBox Use_Same_As_OOT_Process_CB;
        private System.Windows.Forms.CheckBox Disable_Overlay_If_Process_Active_CB;
        private System.Windows.Forms.CheckBox Show_Overlay_During_Rest_CB;
        private System.Windows.Forms.Panel Overlay_Label_BG;
        private System.Windows.Forms.Label Overlay_Label;
        private System.Windows.Forms.TableLayoutPanel Sound_Grid;
        private System.Windows.Forms.CheckBox Disable_Sounds_CB;
        private System.Windows.Forms.CheckBox Play_Rest_Sound_CB;
        private System.Windows.Forms.CheckBox Play_Work_Sound_CB;
        private System.Windows.Forms.Panel Sound_Label_BG;
        private System.Windows.Forms.Label Sound_Label;
        private System.Windows.Forms.FlowLayoutPanel From_To_Flexpanel;
        private System.Windows.Forms.Label From_Label;
        private System.Windows.Forms.DateTimePicker From_DP;
        private System.Windows.Forms.Label To_Label;
        private System.Windows.Forms.DateTimePicker To_DP;
        private System.Windows.Forms.TableLayoutPanel General_Grid;
        private System.Windows.Forms.CheckBox Use_Custom_Value_CB;
        private System.Windows.Forms.CheckBox Close_Btn_Exit_CB;
        private System.Windows.Forms.CheckBox Start_With_Windows_CB;
        private System.Windows.Forms.Panel General_Label_BG;
        private System.Windows.Forms.Label General_Label;
        private System.Windows.Forms.FlowLayoutPanel ExtendValue_FlowPanel;
        private System.Windows.Forms.TextBox Extend_Value_1_TextArea;
        private System.Windows.Forms.TextBox Extend_Value_2_TextArea;
        private System.Windows.Forms.TextBox Extend_Value_3_TextArea;
        private System.Windows.Forms.TableLayoutPanel Overlay_Type_Grid;
        private System.Windows.Forms.Panel Overlay_Type_Label_BG;
        private System.Windows.Forms.Label Overlay_Type_Label;
        private System.Windows.Forms.RadioButton Fullscreen_RB;
        private System.Windows.Forms.RadioButton BigWindowed_RB;
        private System.Windows.Forms.RadioButton SmallWindowed_RB;
        private System.Windows.Forms.Button Close_Btn;
        private System.Windows.Forms.Panel Buttons_Panel;
        private System.Windows.Forms.Button Save_Btn;
        private System.Windows.Forms.Panel Is_This_Honestly_How_One_Makes_A_Space_Between_Buttons_Jesus_Christ;
    }
}