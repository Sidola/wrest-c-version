﻿using System.Drawing;
using System.Windows.Forms;

namespace Wrest
{
    class OverlayMediumView : OverlayBaseFormView
    {
        private Panel textPanel;

        private const int formHeight = 120;
        private const int formWidth = (int) (formHeight * 1.78);
        private const int formPadding = 10;

        public OverlayMediumView(TimerManager timerManager) : base(timerManager)
        {
        }

        internal override void InvalidateTextControl()
        {
            textPanel.Invalidate();
        }

        internal override void SetupBackgroundForm(Form form)
        {
            form.Width = formWidth;
            form.Height = formHeight;

            int y = Screen.PrimaryScreen.WorkingArea.Bottom - form.Height - formPadding;
            int x = Screen.PrimaryScreen.WorkingArea.Right - form.Width - formPadding;

            form.StartPosition = FormStartPosition.Manual;
            form.Location = new Point(x, y);

            form.BackColor = Color.Black;
            form.FormBorderStyle = FormBorderStyle.None;
        }

        internal override void SetupForegroundForm(Form form)
        {
            form.Width = formWidth;
            form.Height = formHeight;

            int y = Screen.PrimaryScreen.WorkingArea.Bottom - form.Height - formPadding;
            int x = Screen.PrimaryScreen.WorkingArea.Right - form.Width - formPadding;

            form.StartPosition = FormStartPosition.Manual;
            form.Location = new Point(x, y);

            form.BackColor = Color.Black;
            form.TransparencyKey = form.BackColor;
            form.FormBorderStyle = FormBorderStyle.None;

            textPanel = new Panel();
            textPanel.MouseDown += Form_Click;

            textPanel.Dock = DockStyle.Fill;
            textPanel.Paint += TextPanel_Paint;

            form.Controls.Add(textPanel);
        }

        private void TextPanel_Paint(object sender, PaintEventArgs e)
        {
            StringDrawUtils.DrawStringOnCenteredOnRectangle(
                currentlyDisplayedText, 30, e.Graphics, textPanel.ClientRectangle);
        }
    }
}
