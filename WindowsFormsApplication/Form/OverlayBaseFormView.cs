﻿using System;
using System.Windows.Forms;

namespace Wrest
{
    abstract class OverlayBaseFormView : FloatingForm
    {
        private Form foregroundForm;
        private TimerManager timerManagerX;

        internal string currentlyDisplayedText;

        internal double targetOpacityBG = 0.8;
        internal double targetOpactiyFG = 1;

        public OverlayBaseFormView(TimerManager timerManagerX)
        {
            this.timerManagerX = timerManagerX;
            Program.GuiRedraw += Program_GuiRedraw;

            MouseDown += Form_Click;
            Shown += OverlayForm_Shown;

            Opacity = 0;
            SetupBackgroundForm(this);

            foregroundForm = new FloatingForm();
            foregroundForm.MouseDown += Form_Click;
            foregroundForm.Opacity = 0;

            SetupForegroundForm(foregroundForm);
        }

        private void Program_GuiRedraw(object sender, EventArgs args)
        {
            if (IsDisposed)
                return;

            if (Opacity < targetOpacityBG)
                Opacity += 0.1;

            // Fades in background first
            if (Opacity > .5 && foregroundForm.Opacity < targetOpactiyFG)
                foregroundForm.Opacity += 0.1;

            if (Visible == false)
                return;

            var remainingMilli = timerManagerX.RemainingTimeMilli;
            var textToDisplay = TimeFormatUtils.FormatMilliAsHHMMSS(remainingMilli);

            if (currentlyDisplayedText != null && currentlyDisplayedText == textToDisplay)
                return;

            currentlyDisplayedText = textToDisplay;
            InvalidateTextControl();
        }

        protected override bool ShowWithoutActivation
        {
            get { return true; }
        }

        private const int WS_EX_TOPMOST = 0x00000008;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams createParams = base.CreateParams;
                createParams.ExStyle |= WS_EX_TOPMOST;
                return createParams;
            }
        }

        public new void Close()
        {
            foregroundForm.Close();
            base.Close();
        }

        private void OverlayForm_Shown(object sender, EventArgs e)
        {
            foregroundForm.Show();
        }

        internal void Form_Click(object sender, EventArgs e)
        {
            Close();
        }

        internal abstract void SetupBackgroundForm(Form form);

        internal abstract void SetupForegroundForm(Form form);

        internal abstract void InvalidateTextControl();

    }
}
