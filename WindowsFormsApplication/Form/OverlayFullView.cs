﻿using System.Drawing;
using System.Windows.Forms;

namespace Wrest
{
    class OverlayFullView : OverlayBaseFormView
    {
        private Panel textPanel;

        public OverlayFullView(TimerManager timerManager) : base(timerManager)
        {
        }

        internal override void InvalidateTextControl()
        {
            textPanel.Invalidate();
        }

        internal override void SetupBackgroundForm(Form form)
        {
            form.WindowState = FormWindowState.Maximized;
            form.BackColor = Color.Black;
            form.FormBorderStyle = FormBorderStyle.None;
        }

        internal override void SetupForegroundForm(Form form)
        {
            form.BackColor = Color.Black;
            form.TransparencyKey = form.BackColor;
            form.WindowState = FormWindowState.Maximized;
            form.FormBorderStyle = FormBorderStyle.None;
            form.TopMost = true;

            textPanel = new Panel();
            textPanel.MouseDown += Form_Click;

            textPanel.Dock = DockStyle.Fill;
            form.Controls.Add(textPanel);

            textPanel.Paint += TextPanel_Paint;
        }

        private void TextPanel_Paint(object sender, PaintEventArgs e)
        {
            StringDrawUtils.DrawStringOnCenteredOnRectangle(
                currentlyDisplayedText, 90, e.Graphics, textPanel.ClientRectangle);
        }
    }
}
