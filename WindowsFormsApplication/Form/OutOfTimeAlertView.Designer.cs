﻿namespace Wrest
{
    partial class OutOfTimeAlertView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TopPanel = new System.Windows.Forms.Panel();
            this.Top_Text_Wrapper_Panel = new System.Windows.Forms.Panel();
            this.Margin_4_Panel = new System.Windows.Forms.Panel();
            this.Time_Remaining_Label = new System.Windows.Forms.Label();
            this.Timer_Is_Running_Out_Label = new System.Windows.Forms.Label();
            this.Margin_1_Panel = new System.Windows.Forms.Panel();
            this.BottomPanel = new System.Windows.Forms.Panel();
            this.Bottom_Button_Flowpanel = new System.Windows.Forms.FlowLayoutPanel();
            this.Extend_Timer_1_Btn = new System.Windows.Forms.Button();
            this.Extend_Timer_2_Btn = new System.Windows.Forms.Button();
            this.Extend_Timer_3_Btn = new System.Windows.Forms.Button();
            this.Stop_Timer_Btn = new System.Windows.Forms.Button();
            this.Bottom_Text_Wrapper_Panel = new System.Windows.Forms.Panel();
            this.Time_Has_Ran_For_Label = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Margin_2_Panel = new System.Windows.Forms.Panel();
            this.White_Panel = new System.Windows.Forms.Panel();
            this.Silver_Panel = new System.Windows.Forms.Panel();
            this.TopPanel.SuspendLayout();
            this.Top_Text_Wrapper_Panel.SuspendLayout();
            this.BottomPanel.SuspendLayout();
            this.Bottom_Button_Flowpanel.SuspendLayout();
            this.Bottom_Text_Wrapper_Panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // TopPanel
            // 
            this.TopPanel.AutoSize = true;
            this.TopPanel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.TopPanel.Controls.Add(this.Top_Text_Wrapper_Panel);
            this.TopPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopPanel.Location = new System.Drawing.Point(0, 0);
            this.TopPanel.Margin = new System.Windows.Forms.Padding(0);
            this.TopPanel.Name = "TopPanel";
            this.TopPanel.Size = new System.Drawing.Size(270, 61);
            this.TopPanel.TabIndex = 0;
            // 
            // Top_Text_Wrapper_Panel
            // 
            this.Top_Text_Wrapper_Panel.AutoSize = true;
            this.Top_Text_Wrapper_Panel.Controls.Add(this.Margin_4_Panel);
            this.Top_Text_Wrapper_Panel.Controls.Add(this.Time_Remaining_Label);
            this.Top_Text_Wrapper_Panel.Controls.Add(this.Timer_Is_Running_Out_Label);
            this.Top_Text_Wrapper_Panel.Controls.Add(this.Margin_1_Panel);
            this.Top_Text_Wrapper_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.Top_Text_Wrapper_Panel.Location = new System.Drawing.Point(0, 0);
            this.Top_Text_Wrapper_Panel.Margin = new System.Windows.Forms.Padding(0);
            this.Top_Text_Wrapper_Panel.Name = "Top_Text_Wrapper_Panel";
            this.Top_Text_Wrapper_Panel.Size = new System.Drawing.Size(270, 61);
            this.Top_Text_Wrapper_Panel.TabIndex = 0;
            // 
            // Margin_4_Panel
            // 
            this.Margin_4_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.Margin_4_Panel.Location = new System.Drawing.Point(0, 51);
            this.Margin_4_Panel.Margin = new System.Windows.Forms.Padding(0);
            this.Margin_4_Panel.Name = "Margin_4_Panel";
            this.Margin_4_Panel.Size = new System.Drawing.Size(270, 10);
            this.Margin_4_Panel.TabIndex = 2;
            // 
            // Time_Remaining_Label
            // 
            this.Time_Remaining_Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.Time_Remaining_Label.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Time_Remaining_Label.Location = new System.Drawing.Point(0, 33);
            this.Time_Remaining_Label.Margin = new System.Windows.Forms.Padding(0);
            this.Time_Remaining_Label.Name = "Time_Remaining_Label";
            this.Time_Remaining_Label.Size = new System.Drawing.Size(270, 18);
            this.Time_Remaining_Label.TabIndex = 2;
            this.Time_Remaining_Label.Text = "00:00:15";
            this.Time_Remaining_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Timer_Is_Running_Out_Label
            // 
            this.Timer_Is_Running_Out_Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.Timer_Is_Running_Out_Label.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Timer_Is_Running_Out_Label.Location = new System.Drawing.Point(0, 10);
            this.Timer_Is_Running_Out_Label.Name = "Timer_Is_Running_Out_Label";
            this.Timer_Is_Running_Out_Label.Size = new System.Drawing.Size(270, 23);
            this.Timer_Is_Running_Out_Label.TabIndex = 0;
            this.Timer_Is_Running_Out_Label.Text = "Timer is running out";
            this.Timer_Is_Running_Out_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Margin_1_Panel
            // 
            this.Margin_1_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.Margin_1_Panel.Location = new System.Drawing.Point(0, 0);
            this.Margin_1_Panel.Margin = new System.Windows.Forms.Padding(0);
            this.Margin_1_Panel.Name = "Margin_1_Panel";
            this.Margin_1_Panel.Size = new System.Drawing.Size(270, 10);
            this.Margin_1_Panel.TabIndex = 1;
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.Bottom_Button_Flowpanel);
            this.BottomPanel.Controls.Add(this.Bottom_Text_Wrapper_Panel);
            this.BottomPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.BottomPanel.Location = new System.Drawing.Point(0, 63);
            this.BottomPanel.Margin = new System.Windows.Forms.Padding(0);
            this.BottomPanel.Name = "BottomPanel";
            this.BottomPanel.Size = new System.Drawing.Size(270, 93);
            this.BottomPanel.TabIndex = 1;
            // 
            // Bottom_Button_Flowpanel
            // 
            this.Bottom_Button_Flowpanel.AutoSize = true;
            this.Bottom_Button_Flowpanel.Controls.Add(this.Extend_Timer_1_Btn);
            this.Bottom_Button_Flowpanel.Controls.Add(this.Extend_Timer_2_Btn);
            this.Bottom_Button_Flowpanel.Controls.Add(this.Extend_Timer_3_Btn);
            this.Bottom_Button_Flowpanel.Controls.Add(this.Stop_Timer_Btn);
            this.Bottom_Button_Flowpanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.Bottom_Button_Flowpanel.Location = new System.Drawing.Point(0, 46);
            this.Bottom_Button_Flowpanel.Margin = new System.Windows.Forms.Padding(0);
            this.Bottom_Button_Flowpanel.Name = "Bottom_Button_Flowpanel";
            this.Bottom_Button_Flowpanel.Padding = new System.Windows.Forms.Padding(10);
            this.Bottom_Button_Flowpanel.Size = new System.Drawing.Size(270, 47);
            this.Bottom_Button_Flowpanel.TabIndex = 1;
            // 
            // Extend_Timer_1_Btn
            // 
            this.Extend_Timer_1_Btn.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Extend_Timer_1_Btn.Location = new System.Drawing.Point(10, 10);
            this.Extend_Timer_1_Btn.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.Extend_Timer_1_Btn.Name = "Extend_Timer_1_Btn";
            this.Extend_Timer_1_Btn.Size = new System.Drawing.Size(55, 27);
            this.Extend_Timer_1_Btn.TabIndex = 0;
            this.Extend_Timer_1_Btn.Text = "5 min";
            this.Extend_Timer_1_Btn.UseVisualStyleBackColor = true;
            this.Extend_Timer_1_Btn.Click += new System.EventHandler(this.Extend_Timer_1_Btn_Click);
            // 
            // Extend_Timer_2_Btn
            // 
            this.Extend_Timer_2_Btn.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Extend_Timer_2_Btn.Location = new System.Drawing.Point(75, 10);
            this.Extend_Timer_2_Btn.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.Extend_Timer_2_Btn.Name = "Extend_Timer_2_Btn";
            this.Extend_Timer_2_Btn.Size = new System.Drawing.Size(55, 27);
            this.Extend_Timer_2_Btn.TabIndex = 1;
            this.Extend_Timer_2_Btn.Text = "15 min";
            this.Extend_Timer_2_Btn.UseVisualStyleBackColor = true;
            this.Extend_Timer_2_Btn.Click += new System.EventHandler(this.Extend_Timer_2_Btn_Click);
            // 
            // Extend_Timer_3_Btn
            // 
            this.Extend_Timer_3_Btn.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Extend_Timer_3_Btn.Location = new System.Drawing.Point(140, 10);
            this.Extend_Timer_3_Btn.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.Extend_Timer_3_Btn.Name = "Extend_Timer_3_Btn";
            this.Extend_Timer_3_Btn.Size = new System.Drawing.Size(55, 27);
            this.Extend_Timer_3_Btn.TabIndex = 2;
            this.Extend_Timer_3_Btn.Text = "30 min";
            this.Extend_Timer_3_Btn.UseVisualStyleBackColor = true;
            this.Extend_Timer_3_Btn.Click += new System.EventHandler(this.Extend_Timer_3_Btn_Click);
            // 
            // Stop_Timer_Btn
            // 
            this.Stop_Timer_Btn.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Stop_Timer_Btn.Location = new System.Drawing.Point(205, 10);
            this.Stop_Timer_Btn.Margin = new System.Windows.Forms.Padding(0);
            this.Stop_Timer_Btn.Name = "Stop_Timer_Btn";
            this.Stop_Timer_Btn.Size = new System.Drawing.Size(55, 27);
            this.Stop_Timer_Btn.TabIndex = 3;
            this.Stop_Timer_Btn.Text = "Stop";
            this.Stop_Timer_Btn.UseVisualStyleBackColor = true;
            this.Stop_Timer_Btn.Click += new System.EventHandler(this.Stop_Timer_Btn_Click);
            // 
            // Bottom_Text_Wrapper_Panel
            // 
            this.Bottom_Text_Wrapper_Panel.AutoSize = true;
            this.Bottom_Text_Wrapper_Panel.Controls.Add(this.Time_Has_Ran_For_Label);
            this.Bottom_Text_Wrapper_Panel.Controls.Add(this.label1);
            this.Bottom_Text_Wrapper_Panel.Controls.Add(this.Margin_2_Panel);
            this.Bottom_Text_Wrapper_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.Bottom_Text_Wrapper_Panel.Location = new System.Drawing.Point(0, 0);
            this.Bottom_Text_Wrapper_Panel.Name = "Bottom_Text_Wrapper_Panel";
            this.Bottom_Text_Wrapper_Panel.Size = new System.Drawing.Size(270, 46);
            this.Bottom_Text_Wrapper_Panel.TabIndex = 0;
            // 
            // Time_Has_Ran_For_Label
            // 
            this.Time_Has_Ran_For_Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.Time_Has_Ran_For_Label.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Time_Has_Ran_For_Label.Location = new System.Drawing.Point(0, 28);
            this.Time_Has_Ran_For_Label.Name = "Time_Has_Ran_For_Label";
            this.Time_Has_Ran_For_Label.Size = new System.Drawing.Size(270, 18);
            this.Time_Has_Ran_For_Label.TabIndex = 2;
            this.Time_Has_Ran_For_Label.Text = "00:27:00";
            this.Time_Has_Ran_For_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(270, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Timer has been running for";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Margin_2_Panel
            // 
            this.Margin_2_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.Margin_2_Panel.Location = new System.Drawing.Point(0, 0);
            this.Margin_2_Panel.Name = "Margin_2_Panel";
            this.Margin_2_Panel.Size = new System.Drawing.Size(270, 10);
            this.Margin_2_Panel.TabIndex = 0;
            // 
            // White_Panel
            // 
            this.White_Panel.BackColor = System.Drawing.Color.White;
            this.White_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.White_Panel.Location = new System.Drawing.Point(0, 61);
            this.White_Panel.Name = "White_Panel";
            this.White_Panel.Size = new System.Drawing.Size(270, 1);
            this.White_Panel.TabIndex = 0;
            // 
            // Silver_Panel
            // 
            this.Silver_Panel.BackColor = System.Drawing.Color.Silver;
            this.Silver_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.Silver_Panel.Location = new System.Drawing.Point(0, 62);
            this.Silver_Panel.Name = "Silver_Panel";
            this.Silver_Panel.Size = new System.Drawing.Size(270, 1);
            this.Silver_Panel.TabIndex = 1;
            // 
            // OutOfTimeAlertView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(270, 155);
            this.ControlBox = false;
            this.Controls.Add(this.BottomPanel);
            this.Controls.Add(this.Silver_Panel);
            this.Controls.Add(this.White_Panel);
            this.Controls.Add(this.TopPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OutOfTimeAlertView";
            this.Opacity = 0D;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.Top_Text_Wrapper_Panel.ResumeLayout(false);
            this.BottomPanel.ResumeLayout(false);
            this.BottomPanel.PerformLayout();
            this.Bottom_Button_Flowpanel.ResumeLayout(false);
            this.Bottom_Text_Wrapper_Panel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel TopPanel;
        private System.Windows.Forms.Panel BottomPanel;
        private System.Windows.Forms.Panel Top_Text_Wrapper_Panel;
        private System.Windows.Forms.Label Timer_Is_Running_Out_Label;
        private System.Windows.Forms.Panel Margin_1_Panel;
        private System.Windows.Forms.Panel White_Panel;
        private System.Windows.Forms.Panel Silver_Panel;
        private System.Windows.Forms.Label Time_Remaining_Label;
        private System.Windows.Forms.Panel Bottom_Text_Wrapper_Panel;
        private System.Windows.Forms.Label Time_Has_Ran_For_Label;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel Margin_2_Panel;
        private System.Windows.Forms.FlowLayoutPanel Bottom_Button_Flowpanel;
        private System.Windows.Forms.Button Extend_Timer_1_Btn;
        private System.Windows.Forms.Button Extend_Timer_2_Btn;
        private System.Windows.Forms.Button Extend_Timer_3_Btn;
        private System.Windows.Forms.Button Stop_Timer_Btn;
        private System.Windows.Forms.Panel Margin_4_Panel;
    }
}