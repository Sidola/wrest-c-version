﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Wrest
{
    internal partial class OutOfTimeAlertView : Form
    {
        private readonly int formPadding = 10;
        private readonly TimerManager timerManager;

        internal OutOfTimeAlertView(TimerManager timerManager)
        {
            this.timerManager = timerManager;

            InitializeComponent();

            if (Program.config.Show_Delay_Options == false)
            {
                BottomPanel.Visible = false;
                Silver_Panel.Visible = false;
                White_Panel.Visible = false;
                Height = 61 + 18;
            }

            MaximumSize = new Size(Width, Height);
            MinimumSize = new Size(Width, Height);

            int y = Screen.PrimaryScreen.WorkingArea.Bottom - Height - formPadding;
            int x = Screen.PrimaryScreen.WorkingArea.Right - Width - formPadding;

            StartPosition = FormStartPosition.Manual;
            Location = new Point(x, y);

            Program.GuiRedraw += Program_GuiRedraw;

            Time_Has_Ran_For_Label.Text = TimeFormatUtils.FormatMilliAsHHMMSS(timerManager.ActiveTimerDuration);

            UpdateExtendTimerButtonsText();
        }

        private void Program_GuiRedraw(object sender, EventArgs args)
        {
            if (IsDisposed)
                return;

            if (Opacity < 1)
                Opacity += 0.1;

            var isRunning = timerManager.IsRunning;

            if (isRunning == false)
            {
                return;
            }

            Time_Remaining_Label.Text = TimeFormatUtils.FormatMilliAsHHMMSS(timerManager.RemainingTimeMilli);
        }

        protected override bool ShowWithoutActivation
        {
            get { return true; }
        }

        private const int WS_EX_TOPMOST = 0x00000008;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams createParams = base.CreateParams;
                createParams.ExStyle |= WS_EX_TOPMOST;
                return createParams;
            }
        }

        private void UpdateExtendTimerButtonsText()
        {
            var config = Program.config;

            var minutes = ConfigManager.GetValueOrDefault(
                config.Use_Custom_Extend_Values,
                config.Custom_Extend_Value_1,
                Properties.Settings.Default.Extend_Value_1);

            Extend_Timer_1_Btn.Text = minutes + " min";

            minutes = ConfigManager.GetValueOrDefault(
                config.Use_Custom_Extend_Values,
                config.Custom_Extend_Value_2,
                Properties.Settings.Default.Extend_Value_2);

            Extend_Timer_2_Btn.Text = minutes + " min";

            minutes = ConfigManager.GetValueOrDefault(
                config.Use_Custom_Extend_Values,
                config.Custom_Extend_Value_3,
                Properties.Settings.Default.Extend_Value_3);

            Extend_Timer_3_Btn.Text = minutes + " min";
        }

        private void Extend_Timer_1_Btn_Click(object sender, EventArgs e)
        {
            var config = Program.config;

            int minutes = ConfigManager.GetValueOrDefault(
                config.Use_Custom_Extend_Values,
                config.Custom_Extend_Value_1,
                Properties.Settings.Default.Extend_Value_1);

            var millis = TimeFormatUtils.MinutesAsMilli(minutes);
            timerManager.IncrementRunningTimerDurationByMilli(millis);
        }

        private void Extend_Timer_2_Btn_Click(object sender, EventArgs e)
        {
            var config = Program.config;

            int minutes = ConfigManager.GetValueOrDefault(
                config.Use_Custom_Extend_Values,
                config.Custom_Extend_Value_2,
                Properties.Settings.Default.Extend_Value_2);

            var millis = TimeFormatUtils.MinutesAsMilli(minutes);
            timerManager.IncrementRunningTimerDurationByMilli(millis);
        }

        private void Extend_Timer_3_Btn_Click(object sender, EventArgs e)
        {
            var config = Program.config;

            int minutes = ConfigManager.GetValueOrDefault(
                config.Use_Custom_Extend_Values,
                config.Custom_Extend_Value_3,
                Properties.Settings.Default.Extend_Value_3);

            var millis = TimeFormatUtils.MinutesAsMilli(minutes);
            timerManager.IncrementRunningTimerDurationByMilli(millis);
        }

        private void Stop_Timer_Btn_Click(object sender, EventArgs e)
        {
            timerManager.Stop();
        }
    }
}
