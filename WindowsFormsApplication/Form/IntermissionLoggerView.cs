﻿using System.Windows.Forms;

namespace Wrest
{
    internal partial class IntermissionLoggerView : Form
    {
        private MainTimerView mainTimerView;

        public IntermissionLoggerView()
        {
            FormClosed += IntermissionLoggerView_FormClosed;
            InitializeComponent();
        }

        public IntermissionLoggerView(MainTimerView mainTimerView) : this()
        {
            this.mainTimerView = mainTimerView;
        }

        private void IntermissionLoggerView_FormClosed(object sender, FormClosedEventArgs e)
        {
            mainTimerView.ResumeTimerChange();
        }

    }
}
