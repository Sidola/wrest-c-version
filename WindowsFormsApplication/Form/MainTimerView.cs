﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Wrest.Util;

namespace Wrest
{
    internal partial class MainTimerView : Form
    {
        // TODO: This is a future feature, should be in the config
        private const bool LOG_AFTER_WORK = false;

        private TimerManager timerManager;

        private StringDrawObject timerNameDrawObject;
        private StringDrawObject timerTimeDrawObject;

        private readonly Color timerIdleColor = Color.FromArgb(150, 0, 0, 0);
        private readonly Color timerActiveColor = Color.FromArgb(240, 0, 0, 0);

        internal MainTimerView(TimerManager timerManager)
        {
            this.timerManager = timerManager;
            this.timerManager.TimerChangedState += TimerManager_TimerChangedState;
            this.timerManager.ActiveTimerChanged += TimerManager_ActiveTimerChanged;

            Program.GuiRedraw += Program_GuiRedraw;

            InitializeComponent();
            Text = Properties.Settings.Default.AppName;

            MinimizeBox = false;
            MaximizeBox = false;

            Resize += MainTimerView_Resize;

            TimerNamePanel.Paint += TimerNamePanel_Paint;
            TimerTextPanel.Paint += TimerTextPanel_Paint;

            var durationSelectors = new List<NumericUpDown>
            {
                WorkHoursSelector,
                WorkMinutesSelector,
                WorkSecondsSelector,
                RestHoursSelector,
                RestMinutesSelector,
                RestSecondsSelector
            };

            foreach (var durationSelector in durationSelectors)
            {
                durationSelector.LostFocus += NumericUpDownNeverEmptyHandler;
                durationSelector.LostFocus += DurationsChangedHandler;

                durationSelector.GotFocus += NumericUpDownSelectOnFocus;
            }

            timerNameDrawObject = new StringDrawObject()
            {
                Text = timerManager.ActiveTimerName,
                Color = timerIdleColor,
                Font = FontManager.AsapFont(35)
            };

            timerTimeDrawObject = new StringDrawObject()
            {
                Text = "00:00:00",
                Color = timerIdleColor,
                Font = FontManager.AsapFont(35)
            };

            UpdateTimerValuesFromConfig();
            EnableTimerConfigurationInput();
            UpdateExtendTimerButtonsText();
        }

        private void TimerTextPanel_Paint(object sender, PaintEventArgs e)
        {
            var clientRectangle = ((Panel)sender).ClientRectangle;
            StringDrawUtils.DrawCenterOnRectangle(timerTimeDrawObject, clientRectangle, e.Graphics);
        }

        private void TimerNamePanel_Paint(object sender, PaintEventArgs e)
        {
            var clientRectangle = ((Panel)sender).ClientRectangle;
            StringDrawUtils.DrawCenterOnRectangle(timerNameDrawObject, clientRectangle, e.Graphics);
        }


        #region // ------- Event Handlers ------- //

        private void Program_GuiRedraw(object sender, EventArgs args)
        {
            RedrawTimerDrawObjects();

            var timerStarted = timerManager.IsStarted;

            if (timerStarted == false)
                return;

            var remainingTimeMilli = timerManager.RemainingTimeMilli;

            if (remainingTimeMilli <= 0)
            {
                // TODO: This is where the logger gets triggered
                PerformTimerExpiredSwitch();
            }

            BlinkTimerIfPaused();
            UpdateTimerTimeLabel();
            UpdateTimerNameLabel();
        }

        private void TimerManager_ActiveTimerChanged(object sender, ActiveTimerChangedArgs args)
        {
            UpdateTimerNameLabel();
        }

        private void TimerManager_TimerChangedState(object sender, TimerChangedStateArgs args)
        {
            switch (args.TimerState)
            {
                case TimerState.BEFORE_STARTED:
                    SetTimerDurations();
                    break;
                case TimerState.AFTER_STARTED:
                    DisableTimerConfigurationInput();
                    break;
                case TimerState.BEFORE_STOPPED:
                    break;
                case TimerState.AFTER_STOPPED:
                    PauseTimer(false);
                    EnableTimerConfigurationInput();
                    break;
                case TimerState.BEORE_PAUSED:
                    break;
                case TimerState.AFTER_PAUSED:
                    PauseTimer(true);
                    break;
                case TimerState.BEFORE_RESUMED:
                    break;
                case TimerState.AFTER_RESUMED:
                    PauseTimer(false);
                    break;
                default:
                    throw new ArgumentException("Unhandled case", args.TimerState.ToString());
            }
        }

        #endregion


        #region // ------- GUI Handlers ------- //

        private void MainTimerView_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                MinimizeToTray();
            }
        }

        private void StartToggleTimerBtn_Click(object sender, EventArgs e)
        {
            var timerStarted = timerManager.IsStarted;

            if (timerStarted)
            {
                timerManager.Stop();
            }
            else
            {
                timerManager.Start();
            }
        }

        private void PauseToggleTimerBtn_Click(object sender, EventArgs e)
        {
            if (timerManager.IsStarted == false)
            {
                throw new InvalidOperationException("Can't pause the timer unless it's started");
            }

            var isTimerUnpaused = timerManager.IsPaused == false;

            if (isTimerUnpaused)
            {
                timerManager.Pause();
            }
            else
            {
                timerManager.Resume();
            }
        }

        private void SwitchTimerBtn_Click(object sender, EventArgs e)
        {
            timerManager.SwitchActiveTimer();
        }

        private void ExtendTimerBtn_Least_Click(object sender, EventArgs e)
        {
            var config = Program.config;

            int minutes = ConfigManager.GetValueOrDefault(
                config.Use_Custom_Extend_Values,
                config.Custom_Extend_Value_1,
                Properties.Settings.Default.Extend_Value_1);

            var millis = TimeFormatUtils.MinutesAsMilli(minutes);
            timerManager.IncrementRunningTimerDurationByMilli(millis);
        }

        private void ExtendTimerBtn_Avarage_Click(object sender, EventArgs e)
        {
            var config = Program.config;

            int minutes = ConfigManager.GetValueOrDefault(
                config.Use_Custom_Extend_Values,
                config.Custom_Extend_Value_2,
                Properties.Settings.Default.Extend_Value_2);

            var millis = TimeFormatUtils.MinutesAsMilli(minutes);
            timerManager.IncrementRunningTimerDurationByMilli(millis);
        }

        private void ExtendTimerBtn_Most_Click(object sender, EventArgs e)
        {
            var config = Program.config;

            int minutes = ConfigManager.GetValueOrDefault(
                config.Use_Custom_Extend_Values,
                config.Custom_Extend_Value_3,
                Properties.Settings.Default.Extend_Value_3);

            var millis = TimeFormatUtils.MinutesAsMilli(minutes);
            timerManager.IncrementRunningTimerDurationByMilli(millis);
        }

        private void NumericUpDownNeverEmptyHandler(object sender, EventArgs e)
        {
            NumericUpDown selector = (NumericUpDown)sender;
            if (selector.Text == "")
            {
                // "Text" is a public property
                selector.Text = "0";
            }
        }

        private void NumericUpDownSelectOnFocus(object sender, EventArgs e)
        {
            NumericUpDown numericUpDown = (NumericUpDown)sender;
            numericUpDown.Select(0, int.MaxValue);
        }

        private void DurationsChangedHandler(object sender, EventArgs e)
        {
            ConfigManager.WriteToDisk();
        }

        #endregion


        #region // ------- Public API ------- //

        internal void UpdateExtendTimerButtonsText()
        {
            var config = Program.config;

            var minutes = ConfigManager.GetValueOrDefault(
                config.Use_Custom_Extend_Values,
                config.Custom_Extend_Value_1,
                Properties.Settings.Default.Extend_Value_1);

            ExtendTimerBtn_Least.Text = minutes + " min";

            minutes = ConfigManager.GetValueOrDefault(
                config.Use_Custom_Extend_Values,
                config.Custom_Extend_Value_2,
                Properties.Settings.Default.Extend_Value_2);

            ExtendTimerBtn_Avarage.Text = minutes + " min";

            minutes = ConfigManager.GetValueOrDefault(
                config.Use_Custom_Extend_Values,
                config.Custom_Extend_Value_3,
                Properties.Settings.Default.Extend_Value_3);

            ExtendTimerBtn_Most.Text = minutes + " min";
        }

        internal string GetRestDurationAsString()
        {
            string restDurationAsString = "";
            restDurationAsString += RestHoursSelector.Value + ":";
            restDurationAsString += RestMinutesSelector.Value + ":";
            restDurationAsString += RestSecondsSelector.Value;

            return restDurationAsString;
        }

        internal string GetWorkDurationAsString()
        {
            string workDurationAsString = "";
            workDurationAsString += WorkHoursSelector.Value + ":";
            workDurationAsString += WorkMinutesSelector.Value + ":";
            workDurationAsString += WorkSecondsSelector.Value;

            return workDurationAsString;
        }

        internal void MinimizeToTray()
        {
            Hide();
            ShowInTaskbar = false;
        }

        internal void RestoreFromTray()
        {
            if (Visible == true)
            {
                return;
            }

            WindowState = FormWindowState.Normal;
            ShowInTaskbar = true;
            Show();
            BringToFront();
        }

        internal void ResumeTimerChange()
        {
            PerformTimerExpiredSwitch();
        }

        internal void SetTimerDurations()
        {
            if (timerManager.IsStarted)
            {
                throw new InvalidOperationException("Cannot set durations while the timer is started");
            }

            timerManager.SetTimerDuration(TimerType.Work, GetTimerValuesAsMilli(TimerType.Work));
            timerManager.SetTimerDuration(TimerType.Rest, GetTimerValuesAsMilli(TimerType.Rest));
        }

        #endregion


        #region // ------- Private API ------- //

        private void RedrawTimerDrawObjects()
        {
            if (timerNameDrawObject.IsDrawn == false)
            {
                // DebugLogger.Log("Drawing name - " + DateTime.Now);
                TimerNamePanel.Invalidate();
            }

            if (timerTimeDrawObject.IsDrawn == false)
            {
                // DebugLogger.Log("Drawing time - " + DateTime.Now);
                TimerTextPanel.Invalidate();
            }
        }

        private void BlinkTimerIfPaused()
        {
            if (timerManager.IsPaused)
            {
                var millis = DateTime.Now.Millisecond;
                var color = timerNameDrawObject.Color;

                if (millis < 500)
                {
                    if (color == timerIdleColor)
                        return;

                    SetTimerLabelsColor(timerIdleColor);
                }
                else
                {
                    if (color == Color.WhiteSmoke)
                        return;

                    SetTimerLabelsColor(Color.WhiteSmoke);
                }
            }
        }

        private void UpdateTimerNameLabel()
        {
            if (Visible == false)
                return;

            var timerName = timerManager.ActiveTimerName;

            if (timerNameDrawObject.Text != timerName)
                timerNameDrawObject.Text = timerName;
        }

        private void UpdateTimerTimeLabel()
        {
            if (Visible == false)
                return;

            var timerTime = TimeFormatUtils.FormatMilliAsHHMMSS(timerManager.RemainingTimeMilli);

            if (timerTimeDrawObject.Text != timerTime)
                timerTimeDrawObject.Text = timerTime;
        }

        private void SetTimerLabelsColor(Color color)
        {
            timerNameDrawObject.Color = color;
            timerTimeDrawObject.Color = color;
        }

        private void PerformTimerExpiredSwitch()
        {
            timerManager.SwitchActiveTimer();
            UpdateTimerNameLabel();
        }

        #endregion


        #region // ------- Gui Util API ------- //

        private void UpdateTimerValuesFromConfig()
        {
            var config = Program.config;

            var workDuration = config.Work_Duration_String.Split(':');
            WorkHoursSelector.Value = int.Parse(workDuration[0]);
            WorkMinutesSelector.Value = int.Parse(workDuration[1]);
            WorkSecondsSelector.Value = int.Parse(workDuration[2]);

            var restDuration = config.Rest_Duration_String.Split(':');
            RestHoursSelector.Value = int.Parse(restDuration[0]);
            RestMinutesSelector.Value = int.Parse(restDuration[1]);
            RestSecondsSelector.Value = int.Parse(restDuration[2]);
        }

        private void PauseTimer(bool pause)
        {
            if (pause)
            {
                DisableTimeIncrementButtons();

                SetTimerLabelsColor(Color.WhiteSmoke);
                SwitchTimerBtn.Enabled = false;
                PauseToggleTimerBtn.Text = "Resume";
            }
            else
            {
                EnableTimeIncrementButtons();

                SetTimerLabelsColor(timerActiveColor);
                SwitchTimerBtn.Enabled = true;
                PauseToggleTimerBtn.Text = "Pause";
            }
        }

        private void DisableTimeIncrementButtons()
        {
            ToggleTimeIncrementButtons(false);
        }

        private void EnableTimeIncrementButtons()
        {
            ToggleTimeIncrementButtons(true);
        }

        private void ToggleTimeIncrementButtons(bool enable)
        {
            ExtendTimerBtn_Least.Enabled = enable;
            ExtendTimerBtn_Avarage.Enabled = enable;
            ExtendTimerBtn_Most.Enabled = enable;
        }

        internal void DisableTimerConfigurationInput()
        {
            ToggleTimerConfigurationInput(false);
        }

        private void EnableTimerConfigurationInput()
        {
            ToggleTimerConfigurationInput(true);
        }

        private void ToggleTimerConfigurationInput(bool enable)
        {
            // Reset timer back to a stopped mode
            if (enable)
            {
                UpdateTimerNameLabel();
                timerTimeDrawObject.Text = "00:00:00";

                StartToggleTimerBtn.Text = "Start";
                SetTimerLabelsColor(timerIdleColor);
            }
            else
            {
                StartToggleTimerBtn.Text = "Stop";
                SetTimerLabelsColor(timerActiveColor);
            }

            WorkHoursSelector.Enabled = enable;
            WorkMinutesSelector.Enabled = enable;
            WorkSecondsSelector.Enabled = enable;

            RestHoursSelector.Enabled = enable;
            RestMinutesSelector.Enabled = enable;
            RestSecondsSelector.Enabled = enable;

            RestDurationLabel.Enabled = enable;
            WorkDurationLabel.Enabled = enable;

            // Beware: Inverted bool below
            PauseToggleTimerBtn.Enabled = !enable;

            ExtendTimerBtn_Least.Enabled = !enable;
            ExtendTimerBtn_Avarage.Enabled = !enable;
            ExtendTimerBtn_Most.Enabled = !enable;
        }

        private long GetTimerValuesAsMilli(TimerType timerType)
        {
            int hours = 0;
            int minutes = 0;
            int seconds = 0;

            switch (timerType)
            {
                case TimerType.Work:
                    hours = (int)WorkHoursSelector.Value;
                    minutes = (int)WorkMinutesSelector.Value;
                    seconds = (int)WorkSecondsSelector.Value;
                    break;

                case TimerType.Rest:
                    hours = (int)RestHoursSelector.Value;
                    minutes = (int)RestMinutesSelector.Value;
                    seconds = (int)RestSecondsSelector.Value;
                    break;

                default:
                    throw new ArgumentException("Unsupported case", timerType.ToString());
            }

            return TimeFormatUtils.ConvertHHMMSSToMilli(hours, minutes, seconds);
        }

        #endregion

    }
}
