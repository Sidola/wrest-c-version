﻿using System.Windows.Forms;

namespace Wrest
{
    /// <summary>
    /// Creates a new form that is hidden from the taskbar
    /// and alt-tab program switcher.
    /// </summary>
    class FloatingForm : Form
    {
        private const int WS_EX_TOOLWINDOW = 0x80; // Hides us from tab switcher
        private const int WS_EX_TOPMOST = 0x00000008;

        public FloatingForm()
        {
            ShowInTaskbar = false;
        }

        // Shows us without activating us
        protected override bool ShowWithoutActivation
        {
            get { return true; }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                var Params = base.CreateParams;
                Params.ExStyle |= (WS_EX_TOOLWINDOW + WS_EX_TOPMOST);
                return Params;
            }
        }

    }
}
