﻿namespace Wrest
{
    partial class MainTimerView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainTimerView));
            this.LeftSide = new System.Windows.Forms.Panel();
            this.TimerNamePanel = new System.Windows.Forms.Panel();
            this.TimerTextPanel = new System.Windows.Forms.Panel();
            this.Margin_1_Panel = new System.Windows.Forms.Panel();
            this.ExtendTimeButtonsLabel = new System.Windows.Forms.Label();
            this.ExtendTimerBtnsGrid = new System.Windows.Forms.TableLayoutPanel();
            this.ExtendTimerBtn_Most = new System.Windows.Forms.Button();
            this.ExtendTimerBtn_Avarage = new System.Windows.Forms.Button();
            this.ExtendTimerBtn_Least = new System.Windows.Forms.Button();
            this.RightSide = new System.Windows.Forms.Panel();
            this.TimerControlBtnsGrid = new System.Windows.Forms.TableLayoutPanel();
            this.StartToggleTimerBtn = new System.Windows.Forms.Button();
            this.PauseToggleTimerBtn = new System.Windows.Forms.Button();
            this.SwitchTimerBtn = new System.Windows.Forms.Button();
            this.RestTimeSelectorsGrid = new System.Windows.Forms.TableLayoutPanel();
            this.RestSecondsSelector = new System.Windows.Forms.NumericUpDown();
            this.RestMinutesSelector = new System.Windows.Forms.NumericUpDown();
            this.RestHoursSelector = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.RestDurationLabel = new System.Windows.Forms.Label();
            this.WorkTimeSelectorsGrid = new System.Windows.Forms.TableLayoutPanel();
            this.WorkHoursSelector = new System.Windows.Forms.NumericUpDown();
            this.WorkSecondsSelector = new System.Windows.Forms.NumericUpDown();
            this.WorkMinutesSelector = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.WorkDurationLabel = new System.Windows.Forms.Label();
            this.Separator_White = new System.Windows.Forms.Panel();
            this.Separator_Silver = new System.Windows.Forms.Panel();
            this.LeftSide.SuspendLayout();
            this.ExtendTimerBtnsGrid.SuspendLayout();
            this.RightSide.SuspendLayout();
            this.TimerControlBtnsGrid.SuspendLayout();
            this.RestTimeSelectorsGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RestSecondsSelector)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RestMinutesSelector)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RestHoursSelector)).BeginInit();
            this.WorkTimeSelectorsGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WorkHoursSelector)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkSecondsSelector)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkMinutesSelector)).BeginInit();
            this.SuspendLayout();
            // 
            // LeftSide
            // 
            this.LeftSide.BackColor = System.Drawing.Color.WhiteSmoke;
            this.LeftSide.Controls.Add(this.TimerNamePanel);
            this.LeftSide.Controls.Add(this.TimerTextPanel);
            this.LeftSide.Controls.Add(this.Margin_1_Panel);
            this.LeftSide.Controls.Add(this.ExtendTimeButtonsLabel);
            this.LeftSide.Controls.Add(this.ExtendTimerBtnsGrid);
            this.LeftSide.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftSide.Location = new System.Drawing.Point(0, 0);
            this.LeftSide.Name = "LeftSide";
            this.LeftSide.Size = new System.Drawing.Size(289, 260);
            this.LeftSide.TabIndex = 0;
            // 
            // TimerNamePanel
            // 
            this.TimerNamePanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TimerNamePanel.Location = new System.Drawing.Point(0, 47);
            this.TimerNamePanel.Name = "TimerNamePanel";
            this.TimerNamePanel.Size = new System.Drawing.Size(289, 50);
            this.TimerNamePanel.TabIndex = 4;
            // 
            // TimerTextPanel
            // 
            this.TimerTextPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TimerTextPanel.Location = new System.Drawing.Point(0, 97);
            this.TimerTextPanel.Name = "TimerTextPanel";
            this.TimerTextPanel.Size = new System.Drawing.Size(289, 50);
            this.TimerTextPanel.TabIndex = 5;
            // 
            // Margin_1_Panel
            // 
            this.Margin_1_Panel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Margin_1_Panel.Location = new System.Drawing.Point(0, 147);
            this.Margin_1_Panel.Name = "Margin_1_Panel";
            this.Margin_1_Panel.Size = new System.Drawing.Size(289, 40);
            this.Margin_1_Panel.TabIndex = 6;
            // 
            // ExtendTimeButtonsLabel
            // 
            this.ExtendTimeButtonsLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ExtendTimeButtonsLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExtendTimeButtonsLabel.ForeColor = System.Drawing.Color.Gray;
            this.ExtendTimeButtonsLabel.Location = new System.Drawing.Point(0, 187);
            this.ExtendTimeButtonsLabel.Margin = new System.Windows.Forms.Padding(0);
            this.ExtendTimeButtonsLabel.Name = "ExtendTimeButtonsLabel";
            this.ExtendTimeButtonsLabel.Padding = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.ExtendTimeButtonsLabel.Size = new System.Drawing.Size(289, 28);
            this.ExtendTimeButtonsLabel.TabIndex = 2;
            this.ExtendTimeButtonsLabel.Text = "Temporarily extend timer";
            this.ExtendTimeButtonsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ExtendTimerBtnsGrid
            // 
            this.ExtendTimerBtnsGrid.AutoSize = true;
            this.ExtendTimerBtnsGrid.ColumnCount = 3;
            this.ExtendTimerBtnsGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.ExtendTimerBtnsGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.ExtendTimerBtnsGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.ExtendTimerBtnsGrid.Controls.Add(this.ExtendTimerBtn_Most, 0, 0);
            this.ExtendTimerBtnsGrid.Controls.Add(this.ExtendTimerBtn_Avarage, 0, 0);
            this.ExtendTimerBtnsGrid.Controls.Add(this.ExtendTimerBtn_Least, 0, 0);
            this.ExtendTimerBtnsGrid.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ExtendTimerBtnsGrid.Location = new System.Drawing.Point(0, 215);
            this.ExtendTimerBtnsGrid.Margin = new System.Windows.Forms.Padding(0);
            this.ExtendTimerBtnsGrid.Name = "ExtendTimerBtnsGrid";
            this.ExtendTimerBtnsGrid.Padding = new System.Windows.Forms.Padding(15, 0, 15, 15);
            this.ExtendTimerBtnsGrid.RowCount = 1;
            this.ExtendTimerBtnsGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ExtendTimerBtnsGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.ExtendTimerBtnsGrid.Size = new System.Drawing.Size(289, 45);
            this.ExtendTimerBtnsGrid.TabIndex = 3;
            // 
            // ExtendTimerBtn_Most
            // 
            this.ExtendTimerBtn_Most.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ExtendTimerBtn_Most.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExtendTimerBtn_Most.Location = new System.Drawing.Point(197, 0);
            this.ExtendTimerBtn_Most.Margin = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.ExtendTimerBtn_Most.Name = "ExtendTimerBtn_Most";
            this.ExtendTimerBtn_Most.Size = new System.Drawing.Size(77, 30);
            this.ExtendTimerBtn_Most.TabIndex = 2;
            this.ExtendTimerBtn_Most.Text = "30min";
            this.ExtendTimerBtn_Most.UseVisualStyleBackColor = true;
            this.ExtendTimerBtn_Most.Click += new System.EventHandler(this.ExtendTimerBtn_Most_Click);
            // 
            // ExtendTimerBtn_Avarage
            // 
            this.ExtendTimerBtn_Avarage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ExtendTimerBtn_Avarage.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExtendTimerBtn_Avarage.Location = new System.Drawing.Point(106, 0);
            this.ExtendTimerBtn_Avarage.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.ExtendTimerBtn_Avarage.Name = "ExtendTimerBtn_Avarage";
            this.ExtendTimerBtn_Avarage.Size = new System.Drawing.Size(76, 30);
            this.ExtendTimerBtn_Avarage.TabIndex = 1;
            this.ExtendTimerBtn_Avarage.Text = "15min";
            this.ExtendTimerBtn_Avarage.UseVisualStyleBackColor = true;
            this.ExtendTimerBtn_Avarage.Click += new System.EventHandler(this.ExtendTimerBtn_Avarage_Click);
            // 
            // ExtendTimerBtn_Least
            // 
            this.ExtendTimerBtn_Least.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ExtendTimerBtn_Least.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExtendTimerBtn_Least.Location = new System.Drawing.Point(15, 0);
            this.ExtendTimerBtn_Least.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.ExtendTimerBtn_Least.Name = "ExtendTimerBtn_Least";
            this.ExtendTimerBtn_Least.Size = new System.Drawing.Size(76, 30);
            this.ExtendTimerBtn_Least.TabIndex = 0;
            this.ExtendTimerBtn_Least.Text = "5min";
            this.ExtendTimerBtn_Least.UseVisualStyleBackColor = true;
            this.ExtendTimerBtn_Least.Click += new System.EventHandler(this.ExtendTimerBtn_Least_Click);
            // 
            // RightSide
            // 
            this.RightSide.BackColor = System.Drawing.SystemColors.Control;
            this.RightSide.Controls.Add(this.TimerControlBtnsGrid);
            this.RightSide.Controls.Add(this.RestTimeSelectorsGrid);
            this.RightSide.Controls.Add(this.WorkTimeSelectorsGrid);
            this.RightSide.Dock = System.Windows.Forms.DockStyle.Top;
            this.RightSide.Location = new System.Drawing.Point(291, 0);
            this.RightSide.Name = "RightSide";
            this.RightSide.Size = new System.Drawing.Size(289, 260);
            this.RightSide.TabIndex = 1;
            // 
            // TimerControlBtnsGrid
            // 
            this.TimerControlBtnsGrid.AutoSize = true;
            this.TimerControlBtnsGrid.ColumnCount = 3;
            this.TimerControlBtnsGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.TimerControlBtnsGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.TimerControlBtnsGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.TimerControlBtnsGrid.Controls.Add(this.StartToggleTimerBtn, 0, 0);
            this.TimerControlBtnsGrid.Controls.Add(this.PauseToggleTimerBtn, 0, 0);
            this.TimerControlBtnsGrid.Controls.Add(this.SwitchTimerBtn, 0, 0);
            this.TimerControlBtnsGrid.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TimerControlBtnsGrid.Location = new System.Drawing.Point(0, 215);
            this.TimerControlBtnsGrid.Margin = new System.Windows.Forms.Padding(0);
            this.TimerControlBtnsGrid.Name = "TimerControlBtnsGrid";
            this.TimerControlBtnsGrid.Padding = new System.Windows.Forms.Padding(15, 0, 15, 15);
            this.TimerControlBtnsGrid.RowCount = 1;
            this.TimerControlBtnsGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TimerControlBtnsGrid.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.TimerControlBtnsGrid.Size = new System.Drawing.Size(289, 45);
            this.TimerControlBtnsGrid.TabIndex = 15;
            // 
            // StartToggleTimerBtn
            // 
            this.StartToggleTimerBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StartToggleTimerBtn.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StartToggleTimerBtn.Location = new System.Drawing.Point(197, 0);
            this.StartToggleTimerBtn.Margin = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.StartToggleTimerBtn.Name = "StartToggleTimerBtn";
            this.StartToggleTimerBtn.Size = new System.Drawing.Size(77, 30);
            this.StartToggleTimerBtn.TabIndex = 2;
            this.StartToggleTimerBtn.Text = "Start";
            this.StartToggleTimerBtn.UseVisualStyleBackColor = true;
            this.StartToggleTimerBtn.Click += new System.EventHandler(this.StartToggleTimerBtn_Click);
            // 
            // PauseToggleTimerBtn
            // 
            this.PauseToggleTimerBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PauseToggleTimerBtn.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PauseToggleTimerBtn.Location = new System.Drawing.Point(106, 0);
            this.PauseToggleTimerBtn.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.PauseToggleTimerBtn.Name = "PauseToggleTimerBtn";
            this.PauseToggleTimerBtn.Size = new System.Drawing.Size(76, 30);
            this.PauseToggleTimerBtn.TabIndex = 1;
            this.PauseToggleTimerBtn.Text = "Pause";
            this.PauseToggleTimerBtn.UseVisualStyleBackColor = true;
            this.PauseToggleTimerBtn.Click += new System.EventHandler(this.PauseToggleTimerBtn_Click);
            // 
            // SwitchTimerBtn
            // 
            this.SwitchTimerBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SwitchTimerBtn.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SwitchTimerBtn.Location = new System.Drawing.Point(15, 0);
            this.SwitchTimerBtn.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.SwitchTimerBtn.Name = "SwitchTimerBtn";
            this.SwitchTimerBtn.Size = new System.Drawing.Size(76, 30);
            this.SwitchTimerBtn.TabIndex = 0;
            this.SwitchTimerBtn.Text = "Switch";
            this.SwitchTimerBtn.UseVisualStyleBackColor = true;
            this.SwitchTimerBtn.Click += new System.EventHandler(this.SwitchTimerBtn_Click);
            // 
            // RestTimeSelectorsGrid
            // 
            this.RestTimeSelectorsGrid.AutoSize = true;
            this.RestTimeSelectorsGrid.ColumnCount = 5;
            this.RestTimeSelectorsGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.55449F));
            this.RestTimeSelectorsGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.168266F));
            this.RestTimeSelectorsGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.55449F));
            this.RestTimeSelectorsGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.168266F));
            this.RestTimeSelectorsGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.55449F));
            this.RestTimeSelectorsGrid.Controls.Add(this.RestSecondsSelector, 5, 1);
            this.RestTimeSelectorsGrid.Controls.Add(this.RestMinutesSelector, 2, 1);
            this.RestTimeSelectorsGrid.Controls.Add(this.RestHoursSelector, 0, 1);
            this.RestTimeSelectorsGrid.Controls.Add(this.label7, 5, 2);
            this.RestTimeSelectorsGrid.Controls.Add(this.label8, 2, 2);
            this.RestTimeSelectorsGrid.Controls.Add(this.label9, 3, 1);
            this.RestTimeSelectorsGrid.Controls.Add(this.label10, 0, 2);
            this.RestTimeSelectorsGrid.Controls.Add(this.label11, 1, 1);
            this.RestTimeSelectorsGrid.Controls.Add(this.RestDurationLabel, 0, 0);
            this.RestTimeSelectorsGrid.Dock = System.Windows.Forms.DockStyle.Top;
            this.RestTimeSelectorsGrid.Location = new System.Drawing.Point(0, 111);
            this.RestTimeSelectorsGrid.Name = "RestTimeSelectorsGrid";
            this.RestTimeSelectorsGrid.Padding = new System.Windows.Forms.Padding(15, 5, 15, 15);
            this.RestTimeSelectorsGrid.RowCount = 3;
            this.RestTimeSelectorsGrid.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.RestTimeSelectorsGrid.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.RestTimeSelectorsGrid.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.RestTimeSelectorsGrid.Size = new System.Drawing.Size(289, 101);
            this.RestTimeSelectorsGrid.TabIndex = 14;
            // 
            // RestSecondsSelector
            // 
            this.RestSecondsSelector.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RestSecondsSelector.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RestSecondsSelector.Location = new System.Drawing.Point(200, 35);
            this.RestSecondsSelector.Name = "RestSecondsSelector";
            this.RestSecondsSelector.Size = new System.Drawing.Size(71, 29);
            this.RestSecondsSelector.TabIndex = 10;
            this.RestSecondsSelector.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.RestSecondsSelector.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // RestMinutesSelector
            // 
            this.RestMinutesSelector.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RestMinutesSelector.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RestMinutesSelector.Location = new System.Drawing.Point(109, 35);
            this.RestMinutesSelector.Name = "RestMinutesSelector";
            this.RestMinutesSelector.Size = new System.Drawing.Size(67, 29);
            this.RestMinutesSelector.TabIndex = 9;
            this.RestMinutesSelector.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // RestHoursSelector
            // 
            this.RestHoursSelector.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RestHoursSelector.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RestHoursSelector.Location = new System.Drawing.Point(18, 35);
            this.RestHoursSelector.Name = "RestHoursSelector";
            this.RestHoursSelector.Size = new System.Drawing.Size(67, 29);
            this.RestHoursSelector.TabIndex = 0;
            this.RestHoursSelector.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Gray;
            this.label7.Location = new System.Drawing.Point(197, 73);
            this.label7.Margin = new System.Windows.Forms.Padding(0, 6, 0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "sec";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Gray;
            this.label8.Location = new System.Drawing.Point(106, 73);
            this.label8.Margin = new System.Windows.Forms.Padding(0, 6, 0, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "min";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(182, 32);
            this.label9.Name = "label9";
            this.label9.Padding = new System.Windows.Forms.Padding(1, 0, 0, 5);
            this.label9.Size = new System.Drawing.Size(12, 35);
            this.label9.TabIndex = 8;
            this.label9.Text = ":";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Gray;
            this.label10.Location = new System.Drawing.Point(15, 73);
            this.label10.Margin = new System.Windows.Forms.Padding(0, 6, 0, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "hour";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(91, 32);
            this.label11.Name = "label11";
            this.label11.Padding = new System.Windows.Forms.Padding(1, 0, 0, 5);
            this.label11.Size = new System.Drawing.Size(12, 35);
            this.label11.TabIndex = 2;
            this.label11.Text = ":";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RestDurationLabel
            // 
            this.RestDurationLabel.AutoSize = true;
            this.RestTimeSelectorsGrid.SetColumnSpan(this.RestDurationLabel, 5);
            this.RestDurationLabel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RestDurationLabel.Location = new System.Drawing.Point(15, 5);
            this.RestDurationLabel.Margin = new System.Windows.Forms.Padding(0);
            this.RestDurationLabel.Name = "RestDurationLabel";
            this.RestDurationLabel.Padding = new System.Windows.Forms.Padding(0, 0, 0, 7);
            this.RestDurationLabel.Size = new System.Drawing.Size(106, 27);
            this.RestDurationLabel.TabIndex = 13;
            this.RestDurationLabel.Text = "Rest Duration";
            this.RestDurationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // WorkTimeSelectorsGrid
            // 
            this.WorkTimeSelectorsGrid.AutoSize = true;
            this.WorkTimeSelectorsGrid.ColumnCount = 5;
            this.WorkTimeSelectorsGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.55449F));
            this.WorkTimeSelectorsGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.168266F));
            this.WorkTimeSelectorsGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.55449F));
            this.WorkTimeSelectorsGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.168266F));
            this.WorkTimeSelectorsGrid.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.55449F));
            this.WorkTimeSelectorsGrid.Controls.Add(this.WorkHoursSelector, 0, 1);
            this.WorkTimeSelectorsGrid.Controls.Add(this.WorkSecondsSelector, 5, 1);
            this.WorkTimeSelectorsGrid.Controls.Add(this.WorkMinutesSelector, 2, 1);
            this.WorkTimeSelectorsGrid.Controls.Add(this.label1, 2, 2);
            this.WorkTimeSelectorsGrid.Controls.Add(this.label5, 5, 2);
            this.WorkTimeSelectorsGrid.Controls.Add(this.label3, 3, 1);
            this.WorkTimeSelectorsGrid.Controls.Add(this.label4, 0, 2);
            this.WorkTimeSelectorsGrid.Controls.Add(this.label2, 1, 1);
            this.WorkTimeSelectorsGrid.Controls.Add(this.WorkDurationLabel, 0, 0);
            this.WorkTimeSelectorsGrid.Dock = System.Windows.Forms.DockStyle.Top;
            this.WorkTimeSelectorsGrid.Location = new System.Drawing.Point(0, 0);
            this.WorkTimeSelectorsGrid.Name = "WorkTimeSelectorsGrid";
            this.WorkTimeSelectorsGrid.Padding = new System.Windows.Forms.Padding(15);
            this.WorkTimeSelectorsGrid.RowCount = 3;
            this.WorkTimeSelectorsGrid.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.WorkTimeSelectorsGrid.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.WorkTimeSelectorsGrid.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.WorkTimeSelectorsGrid.Size = new System.Drawing.Size(289, 111);
            this.WorkTimeSelectorsGrid.TabIndex = 5;
            // 
            // WorkHoursSelector
            // 
            this.WorkHoursSelector.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WorkHoursSelector.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WorkHoursSelector.Location = new System.Drawing.Point(18, 45);
            this.WorkHoursSelector.Name = "WorkHoursSelector";
            this.WorkHoursSelector.Size = new System.Drawing.Size(67, 29);
            this.WorkHoursSelector.TabIndex = 0;
            this.WorkHoursSelector.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // WorkSecondsSelector
            // 
            this.WorkSecondsSelector.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WorkSecondsSelector.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WorkSecondsSelector.Location = new System.Drawing.Point(200, 45);
            this.WorkSecondsSelector.Name = "WorkSecondsSelector";
            this.WorkSecondsSelector.Size = new System.Drawing.Size(71, 29);
            this.WorkSecondsSelector.TabIndex = 10;
            this.WorkSecondsSelector.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.WorkSecondsSelector.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // WorkMinutesSelector
            // 
            this.WorkMinutesSelector.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WorkMinutesSelector.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WorkMinutesSelector.Location = new System.Drawing.Point(109, 45);
            this.WorkMinutesSelector.Name = "WorkMinutesSelector";
            this.WorkMinutesSelector.Size = new System.Drawing.Size(67, 29);
            this.WorkMinutesSelector.TabIndex = 9;
            this.WorkMinutesSelector.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(106, 83);
            this.label1.Margin = new System.Windows.Forms.Padding(0, 6, 0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "min";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Gray;
            this.label5.Location = new System.Drawing.Point(197, 83);
            this.label5.Margin = new System.Windows.Forms.Padding(0, 6, 0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "sec";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(182, 42);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(1, 0, 0, 5);
            this.label3.Size = new System.Drawing.Size(12, 35);
            this.label3.TabIndex = 8;
            this.label3.Text = ":";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Gray;
            this.label4.Location = new System.Drawing.Point(15, 83);
            this.label4.Margin = new System.Windows.Forms.Padding(0, 6, 0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "hour";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(91, 42);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(1, 0, 0, 5);
            this.label2.Size = new System.Drawing.Size(12, 35);
            this.label2.TabIndex = 2;
            this.label2.Text = ":";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // WorkDurationLabel
            // 
            this.WorkDurationLabel.AutoSize = true;
            this.WorkTimeSelectorsGrid.SetColumnSpan(this.WorkDurationLabel, 5);
            this.WorkDurationLabel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WorkDurationLabel.Location = new System.Drawing.Point(15, 15);
            this.WorkDurationLabel.Margin = new System.Windows.Forms.Padding(0);
            this.WorkDurationLabel.Name = "WorkDurationLabel";
            this.WorkDurationLabel.Padding = new System.Windows.Forms.Padding(0, 0, 0, 7);
            this.WorkDurationLabel.Size = new System.Drawing.Size(113, 27);
            this.WorkDurationLabel.TabIndex = 13;
            this.WorkDurationLabel.Text = "Work Duration";
            this.WorkDurationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Separator_White
            // 
            this.Separator_White.BackColor = System.Drawing.Color.White;
            this.Separator_White.Dock = System.Windows.Forms.DockStyle.Left;
            this.Separator_White.Location = new System.Drawing.Point(289, 0);
            this.Separator_White.Name = "Separator_White";
            this.Separator_White.Size = new System.Drawing.Size(1, 260);
            this.Separator_White.TabIndex = 1;
            // 
            // Separator_Silver
            // 
            this.Separator_Silver.BackColor = System.Drawing.Color.Silver;
            this.Separator_Silver.Dock = System.Windows.Forms.DockStyle.Left;
            this.Separator_Silver.Location = new System.Drawing.Point(290, 0);
            this.Separator_Silver.Name = "Separator_Silver";
            this.Separator_Silver.Size = new System.Drawing.Size(1, 260);
            this.Separator_Silver.TabIndex = 2;
            // 
            // MainTimerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 260);
            this.Controls.Add(this.RightSide);
            this.Controls.Add(this.Separator_Silver);
            this.Controls.Add(this.Separator_White);
            this.Controls.Add(this.LeftSide);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainTimerView";
            this.Text = "Timer";
            this.LeftSide.ResumeLayout(false);
            this.LeftSide.PerformLayout();
            this.ExtendTimerBtnsGrid.ResumeLayout(false);
            this.RightSide.ResumeLayout(false);
            this.RightSide.PerformLayout();
            this.TimerControlBtnsGrid.ResumeLayout(false);
            this.RestTimeSelectorsGrid.ResumeLayout(false);
            this.RestTimeSelectorsGrid.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RestSecondsSelector)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RestMinutesSelector)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RestHoursSelector)).EndInit();
            this.WorkTimeSelectorsGrid.ResumeLayout(false);
            this.WorkTimeSelectorsGrid.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WorkHoursSelector)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkSecondsSelector)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkMinutesSelector)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel LeftSide;
        private System.Windows.Forms.Panel RightSide;
        private System.Windows.Forms.Panel Separator_White;
        private System.Windows.Forms.Panel Separator_Silver;
        private System.Windows.Forms.Label ExtendTimeButtonsLabel;
        private System.Windows.Forms.TableLayoutPanel ExtendTimerBtnsGrid;
        private System.Windows.Forms.Button ExtendTimerBtn_Least;
        private System.Windows.Forms.Button ExtendTimerBtn_Most;
        private System.Windows.Forms.Button ExtendTimerBtn_Avarage;
        private System.Windows.Forms.TableLayoutPanel WorkTimeSelectorsGrid;
        private System.Windows.Forms.NumericUpDown WorkHoursSelector;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown WorkSecondsSelector;
        private System.Windows.Forms.NumericUpDown WorkMinutesSelector;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label WorkDurationLabel;
        private System.Windows.Forms.TableLayoutPanel RestTimeSelectorsGrid;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown RestSecondsSelector;
        private System.Windows.Forms.NumericUpDown RestMinutesSelector;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown RestHoursSelector;
        private System.Windows.Forms.Label RestDurationLabel;
        private System.Windows.Forms.TableLayoutPanel TimerControlBtnsGrid;
        private System.Windows.Forms.Button StartToggleTimerBtn;
        private System.Windows.Forms.Button PauseToggleTimerBtn;
        private System.Windows.Forms.Button SwitchTimerBtn;
        private System.Windows.Forms.Panel TimerNamePanel;
        private System.Windows.Forms.Panel TimerTextPanel;
        private System.Windows.Forms.Panel Margin_1_Panel;
    }
}