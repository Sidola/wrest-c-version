﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Wrest
{
    public partial class ConfigView : Form
    {
        public ConfigView()
        {
            InitializeComponent();
            Text = Properties.Settings.Default.AppName + " - Settings";

            SetupViewFromConfig();
        }

        private void SetupViewFromConfig()
        {
            var config = Program.config;

            #region OOT Alert

            OOT_Show_Alert_CB.Checked = config.Show_Out_Of_Time_Alert;
            OOT_Show_Delay_CB.Checked = config.Show_Delay_Options;
            OOT_Disable_If_Proc_Active_CB.Checked = config.Disable_Alert_If_Proc_Active;

            // If input not enabled via CB, disable it
            // Remember to disable labels too
            if (OOT_Disable_If_Proc_Active_CB.Checked == false)
            {
                OOT_Disable_Proc_Label.Enabled = false;
                OOT_Disable_Proc_TextArea.Enabled = false;
            }

            // If empty, disable and set dummy values
            if (string.IsNullOrEmpty(config.OOT_Proc_CsvList))
            {
                OOT_Disable_If_Proc_Active_CB.Checked = false;

                OOT_Disable_Proc_Label.Enabled = false;
                OOT_Disable_Proc_TextArea.Enabled = false;
            }
            else
            {
                OOT_Disable_Proc_TextArea.Text = config.OOT_Proc_CsvList;
            }


            #endregion


            #region Overlay

            Show_Overlay_During_Rest_CB.Checked = config.Show_Overlay_During_Rest;
            Disable_Overlay_If_Process_Active_CB.Checked = config.Disable_Overlay_If_Proc_Active;
            Use_Same_As_OOT_Process_CB.Checked = config.Use_OOT_Proc_List_For_Overlay;

            // If input not enabled via CB, disable it
            if (Disable_Overlay_If_Process_Active_CB.Checked == false)
            {
                Use_Same_As_OOT_Process_CB.Enabled = false;

                Overlay_Disable_Proc_Label.Enabled = false;
                Overlay_Disable_Proc_TextArea.Enabled = false;
            }

            // If same as OOT, disable input
            if (Use_Same_As_OOT_Process_CB.Checked)
            {
                Overlay_Disable_Proc_Label.Enabled = false;
                Overlay_Disable_Proc_TextArea.Enabled = false;
            }

            // If empty, disable and set dummy values
            if (string.IsNullOrEmpty(config.Overlay_Proc_CsvList))
            {
                if (Use_Same_As_OOT_Process_CB.Checked == false)
                {
                    Disable_Overlay_If_Process_Active_CB.Checked = false;

                    Use_Same_As_OOT_Process_CB.Enabled = false;

                    Overlay_Disable_Proc_Label.Enabled = false;
                    Overlay_Disable_Proc_TextArea.Enabled = false;
                }
            }
            else
            {
                Overlay_Disable_Proc_TextArea.Text = config.Overlay_Proc_CsvList;
            }


            #endregion


            #region Overlay Type

            switch (config.Overlay_Type)
            {
                case OverlayType.FULLSCREEN:
                    Fullscreen_RB.Checked = true;
                    break;
                case OverlayType.BIG_WINDOWED:
                    BigWindowed_RB.Checked = true;
                    break;
                case OverlayType.SMALL_WINDOWED:
                    SmallWindowed_RB.Checked = true;
                    break;
                default:
                    throw new ArgumentException("Unhandled case", config.Overlay_Type.ToString());
            }

            #endregion


            #region Sound

            Play_Work_Sound_CB.Checked = config.Play_Sound_Work_Ends;
            Play_Rest_Sound_CB.Checked = config.Play_Sound_Rest_Ends;
            Disable_Sounds_CB.Checked = config.Disable_Sound_During_Hours;

            // If above checkbox isn't checked, disable controls and labels
            if (Disable_Sounds_CB.Checked == false)
            {
                From_Label.Enabled = false;
                From_DP.Enabled = false;

                To_Label.Enabled = false;
                To_DP.Enabled = false;
            }
            
            if (string.IsNullOrEmpty(config.Disable_From_Hours) 
            || string.IsNullOrEmpty(config.Disable_To_Hours))
            {
                From_DP.Text = "23:00";
                To_DP.Text = "07:00";
            }
            else
            {
                From_DP.Text = config.Disable_From_Hours;
                To_DP.Text = config.Disable_To_Hours;
            }

            #endregion


            #region General

            Start_With_Windows_CB.Checked = config.Auto_Start_With_Windows;
            Close_Btn_Exit_CB.Checked = config.Close_Button_Exits;
            Use_Custom_Value_CB.Checked = config.Use_Custom_Extend_Values;

            // If above checkbox isn't checked, disable inputs

            if (Use_Custom_Value_CB.Checked == false)
            {
                Extend_Value_1_TextArea.Enabled = false;
                Extend_Value_2_TextArea.Enabled = false;
                Extend_Value_3_TextArea.Enabled = false;
            }

            if (string.IsNullOrEmpty(config.Custom_Extend_Value_1.ToString())
            || string.IsNullOrEmpty(config.Custom_Extend_Value_2.ToString())
            || string.IsNullOrEmpty(config.Custom_Extend_Value_3.ToString()))
            {
                Extend_Value_1_TextArea.Text = "5";
                Extend_Value_2_TextArea.Text = "15";
                Extend_Value_3_TextArea.Text = "30";
            }
            else
            {
                Extend_Value_1_TextArea.Text = config.Custom_Extend_Value_1.ToString();
                Extend_Value_2_TextArea.Text = config.Custom_Extend_Value_2.ToString();
                Extend_Value_3_TextArea.Text = config.Custom_Extend_Value_3.ToString();
            }

            #endregion
        }

        #region Handlers - OOT Alerts

        private void OOT_Disable_If_Proc_Active_CB_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox thisCheckbox = (CheckBox) sender;
            OOT_Disable_Proc_TextArea.Enabled = thisCheckbox.Checked;
            OOT_Disable_Proc_Label.Enabled = thisCheckbox.Checked;
        }

        #endregion

        #region Handlers - Overlay

        private void Disable_OOT_If_Process_Active_CB_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox thisCheckbox = (CheckBox) sender;
            Use_Same_As_OOT_Process_CB.Enabled = thisCheckbox.Checked;

            if (thisCheckbox.Checked == false)
            {
                Overlay_Disable_Proc_Label.Enabled = false;
                Overlay_Disable_Proc_TextArea.Enabled = false;
            }
            else
            {
                // Beware, inverted bools
                Overlay_Disable_Proc_Label.Enabled = !Use_Same_As_OOT_Process_CB.Checked;
                Overlay_Disable_Proc_TextArea.Enabled = !Use_Same_As_OOT_Process_CB.Checked;
            }
        }

        private void Use_Same_As_OOT_Process_CB_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox thisCheckbox = (CheckBox) sender;

            // Beware, inverted bools
            Overlay_Disable_Proc_Label.Enabled = !thisCheckbox.Checked;
            Overlay_Disable_Proc_TextArea.Enabled = !thisCheckbox.Checked;
        }

        #endregion

        #region Handlers - Sound

        private void Disable_Sounds_CB_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox thisCheckbox = (CheckBox)sender;

            From_Label.Enabled = thisCheckbox.Checked;
            From_DP.Enabled = thisCheckbox.Checked;
            To_Label.Enabled = thisCheckbox.Checked;
            To_DP.Enabled = thisCheckbox.Checked;
        }

        #endregion

        #region Handlers - General

        private void Use_Custom_Value_CB_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox thisCheckbox = (CheckBox)sender;

            Extend_Value_1_TextArea.Enabled = thisCheckbox.Checked;
            Extend_Value_2_TextArea.Enabled = thisCheckbox.Checked;
            Extend_Value_3_TextArea.Enabled = thisCheckbox.Checked;
        }

        #endregion

        private void UpdateConfigFromGui()
        {
            var config = Program.config;

            #region OOT Alert

            config.Show_Out_Of_Time_Alert = OOT_Show_Alert_CB.Checked;
            config.Show_Delay_Options = OOT_Show_Delay_CB.Checked;
            config.Disable_Alert_If_Proc_Active = OOT_Disable_If_Proc_Active_CB.Checked;

            // Validation candidate
            // #getCsvOrEmptyString(string): string
            config.OOT_Proc_CsvList = OOT_Disable_Proc_TextArea.Text;

            #endregion


            #region Overlay

            config.Show_Overlay_During_Rest = Show_Overlay_During_Rest_CB.Checked;
            config.Disable_Overlay_If_Proc_Active = Disable_Overlay_If_Process_Active_CB.Checked;
            config.Use_OOT_Proc_List_For_Overlay = Use_Same_As_OOT_Process_CB.Checked;

            // Validation candidate
            // #getCsvOrEmptyString(string): string
            config.Overlay_Proc_CsvList = Overlay_Disable_Proc_TextArea.Text;

            #endregion


            #region Overlay Type

            RadioButton selectedRadioButton = Overlay_Type_Grid.Controls.OfType<RadioButton>()
                .FirstOrDefault(r => r.Checked);

            // TODO: I don't like this, I'd rather have enum values
            // assoicated with each RB
            switch (selectedRadioButton.Text)
            {
                case "Fullscreen overlay":
                    config.Overlay_Type = OverlayType.FULLSCREEN;
                    break;
                case "Big windowed overlay":
                    config.Overlay_Type = OverlayType.BIG_WINDOWED;
                    break;
                case "Small windowed overlay":
                    config.Overlay_Type = OverlayType.SMALL_WINDOWED;
                    break;
                default:
                    throw new ArgumentException("Unhandled case", selectedRadioButton.Text);
            }

            #endregion


            #region Sound

            config.Play_Sound_Work_Ends = Play_Work_Sound_CB.Checked;
            config.Play_Sound_Rest_Ends = Play_Rest_Sound_CB.Checked;
            config.Disable_Sound_During_Hours = Disable_Sounds_CB.Checked;

            config.Disable_From_Hours = From_DP.Text;
            config.Disable_To_Hours = To_DP.Text;

            #endregion


            #region General

            config.Auto_Start_With_Windows = Start_With_Windows_CB.Checked;
            config.Close_Button_Exits = Close_Btn_Exit_CB.Checked;
            config.Use_Custom_Extend_Values = Use_Custom_Value_CB.Checked;

            // TODO: Validation candidates
            // #getAsIntOrDefault(string, "default"): string
            config.Custom_Extend_Value_1 = int.Parse(Extend_Value_1_TextArea.Text);
            config.Custom_Extend_Value_2 = int.Parse(Extend_Value_2_TextArea.Text);
            config.Custom_Extend_Value_3 = int.Parse(Extend_Value_3_TextArea.Text);

            #endregion
        }

        private void Save_Btn_Click(object sender, EventArgs e)
        {
            ConfigManager.WriteToDisk();
            UpdateConfigFromGui();
            Close();
        }

        private void Close_Btn_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
