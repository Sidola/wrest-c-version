﻿namespace Wrest
{
    partial class IntermissionLoggerView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DoneBtn = new System.Windows.Forms.Button();
            this.LogTextInput = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // DoneBtn
            // 
            this.DoneBtn.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.DoneBtn.Location = new System.Drawing.Point(10, 334);
            this.DoneBtn.Name = "DoneBtn";
            this.DoneBtn.Size = new System.Drawing.Size(403, 23);
            this.DoneBtn.TabIndex = 0;
            this.DoneBtn.Text = "Done";
            this.DoneBtn.UseVisualStyleBackColor = true;
            // 
            // LogTextInput
            // 
            this.LogTextInput.Dock = System.Windows.Forms.DockStyle.Top;
            this.LogTextInput.Location = new System.Drawing.Point(10, 10);
            this.LogTextInput.Margin = new System.Windows.Forms.Padding(0);
            this.LogTextInput.Name = "LogTextInput";
            this.LogTextInput.Size = new System.Drawing.Size(403, 313);
            this.LogTextInput.TabIndex = 1;
            this.LogTextInput.Text = "";
            // 
            // IntermissionLoggerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(423, 367);
            this.Controls.Add(this.LogTextInput);
            this.Controls.Add(this.DoneBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "IntermissionLoggerView";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.Text = "IntermissionLoggerView";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button DoneBtn;
        private System.Windows.Forms.RichTextBox LogTextInput;
    }
}