﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wrest.Util
{
    internal class DebugLogger
    {
        internal static void Log(string message)
        {
            Debug.WriteLine("");
            Debug.WriteLine("================================================");
            Debug.WriteLine($"{message}");
            Debug.WriteLine("================================================");
            Debug.WriteLine("");
        }

        internal static void LogIf(bool condition, string message)
        {
            if (condition)
            {
                Log(message);
            }
        }

    }
}
