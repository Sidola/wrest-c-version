﻿namespace Wrest
{
    public class TimeFormatUtils
    {
        public static string FormatMilliAsHHMMSS(long milli)
        {
            int seconds = (int)(milli / 1000) % 60;
            int minutes = (int)((milli / (1000 * 60)) % 60);
            int hours = (int)((milli / (1000 * 60 * 60)) % 24);

            string secondsString = seconds.ToString().PadLeft(2, '0');
            string minutesString = minutes.ToString().PadLeft(2, '0');
            string hoursString = hours.ToString().PadLeft(2, '0');

            return string.Format("{0}:{1}:{2}", hoursString, minutesString, secondsString);
        }

        internal static int MinutesAsMilli(int minutes)
        {
            return minutes * 60000;
        }

        internal static long ConvertHHMMSSToMilli(int hours, int minutes, int seconds)
        {
            int milli = 0;
            milli += (hours * 60 * 60 * 1000);
            milli += (minutes * 60 * 1000);
            milli += (seconds * 1000);
            return milli;
        }
    }
}
