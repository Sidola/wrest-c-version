﻿using System;
using System.Drawing;
using System.Drawing.Text;

namespace Wrest
{
    internal class StringDrawUtils
    {
        internal static void DrawStringOnCenteredOnRectangle(string stringToDraw, int fontSize, Graphics graphics, RectangleF clientRectangle)
        {
            graphics.TextRenderingHint = TextRenderingHint.AntiAlias;

            string drawString = stringToDraw;
            Font drawFont = FontManager.AsapFont(fontSize);
            SolidBrush drawBrush = new SolidBrush(Color.White);
            StringFormat drawFormat = new StringFormat();
            drawFormat.LineAlignment = StringAlignment.Center;
            drawFormat.Alignment = StringAlignment.Center;

            // float x = 150.0F;
            // float y = 50.0F;
            // formGraphics.DrawString(drawString, drawFont, drawBrush, x, y, drawFormat);

            graphics.DrawString(drawString, drawFont, drawBrush, clientRectangle, drawFormat);

            drawFont.Dispose();
            drawBrush.Dispose();
            graphics.Dispose();
        }

        internal static void DrawValueCenterOnRectangle(string valueToDraw, Rectangle clientRectangle, Graphics graphics, Font font, Color color)
        {
            graphics.TextRenderingHint = TextRenderingHint.AntiAlias;

            StringFormat stringFormat = new StringFormat();
            stringFormat.LineAlignment = StringAlignment.Center;
            stringFormat.Alignment = StringAlignment.Center;

            SolidBrush brush = new SolidBrush(color);

            graphics.DrawString(valueToDraw, font, brush, clientRectangle, stringFormat);

            // Dispose of the evidence
            font.Dispose();
            brush.Dispose();
            graphics.Dispose();
        }

        internal static void DrawCenterOnRectangle(StringDrawObject drawObject, Rectangle clientRectangle, Graphics graphics)
        {
            graphics.TextRenderingHint = TextRenderingHint.AntiAlias;

            StringFormat stringFormat = new StringFormat();
            stringFormat.LineAlignment = StringAlignment.Center;
            stringFormat.Alignment = StringAlignment.Center;

            SolidBrush brush = new SolidBrush(drawObject.Color);

            graphics.DrawString(drawObject.Text, drawObject.Font, brush, clientRectangle, stringFormat);

            drawObject.Drawn();

            // Dispose of the evidence
            brush.Dispose();
            graphics.Dispose();
        }
    }
}
