﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Wrest.Util;

namespace Wrest
{
    internal class ProcessUtils
    {
        [DllImport("user32.dll")]
        public static extern IntPtr GetWindowThreadProcessId(IntPtr hWnd, out uint ProcessId);

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        private static string GetActiveProcessFileName()
        {
            try
            {
                IntPtr hwnd = GetForegroundWindow();
                uint pid;
                GetWindowThreadProcessId(hwnd, out pid);
                Process p = Process.GetProcessById((int)pid);

                return p.MainModule.ModuleName;
            }
            catch (Exception e)
            {
                DebugLogger.LogIf(Program.isDebugMode, $"Could not get active process name - {e.Message}");
                return "COULD-NOT-GET-PROCESS";
            }
        }

        internal static bool IsActiveProcessPresentInList(List<string> processList)
        {
            return processList.Contains(GetActiveProcessFileName());
        }

    }
}
