﻿using System.Drawing;

namespace Wrest
{
    /// <summary>
    /// Wraps data required to draw strings on the GUI. Use when you need to
    /// be able to modify the state of a string that is being draw repeatedly.
    /// 
    /// Note, this object is super duper not thread safe. I think. I'm not
    /// sure. Programming is hard.
    /// </summary>
    internal class StringDrawObject
    {
        private string text;
        private Color color;
        private Font font;
        private bool drawn = false;

        internal string Text
        {
            get { return text; }
            set { text = value; drawn = false; }
        }

        internal Color Color
        {
            get { return color;  }
            set { color = value; drawn = false; }
        }

        internal Font Font
        {
            get { return font; }
            set { font = value; drawn = false; }
        }

        /// <summary>
        /// Returns true if the latest set data has been
        /// drawn.
        /// </summary>
        internal bool IsDrawn { get { return drawn; } }

        /// <summary>
        /// Called by whoever draws this object to signal
        /// that the data in this object has been drawn.
        /// </summary>
        internal void Drawn()
        {
            drawn = true;
        }
    }
}
