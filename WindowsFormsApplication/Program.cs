﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Text;
using System.Threading;
using System.Windows.Forms;
using Wrest.Util;
using wyDay.Controls;
using WForms = System.Windows.Forms;

namespace Wrest
{
    internal delegate void GuiRedrawEventHandler(object sender, EventArgs args);

    internal class Program
    {
        private const int OOT_DELAY_DISPLAY_THRESHOLD_MS = 15000;

        private static readonly Mutex mutex = new Mutex(true, "{98175805-5069-4DE3-90B0-C0036B4F6C7B}");
        private static readonly PrivateFontCollection PRIVATE_FONT_COLLECTION = new PrivateFontCollection();

        private static IntermissionLoggerView intermissionLoggerView;
        private static MainTimerView mainTimerView;
        private static OverlayBaseFormView overlayForm;
        private static OutOfTimeAlertView outOfTimeAlertView;
        private static ConfigView configView;

        private static WForms.Timer guiRedrawTimer;

        private static NotifyIcon notifyIcon;
        private static MenuItem toggleTimerItem;
        private static MenuItem timerTimeItem;

        private static TimerManager timerManager;

        internal static Config config;
        internal static bool isDebugMode = false;

        private static SoundManager soundManager;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            #if DEBUG
            isDebugMode = true;
            #endif
            
            if (mutex.WaitOne(TimeSpan.Zero, true))
            {
                SetupApplication();
                mutex.ReleaseMutex();
            }
            else
            {
                // TODO: This should activate the currently running version
                // and bring it back from the tray if it's minimized
                return;
            }
        }


        #region // ------- Setup ------- //

        private static void SetupApplication()
        {
            config = ConfigManager.ReadFromDisk();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            SetupNotifyIcon();
            SetupContextMenu();

            // In case the application was moved
            UpdateAutostartRegKey();

            timerManager = new TimerManager();
            timerManager.ActiveTimerChanged += TimerManager_ActiveTimerChanged;
            timerManager.TimerChangedState += TimerManager_TimerChangedState;
            timerManager.ActiveTimerTick += TimerManager_ActiveTimerTick;

            mainTimerView = new MainTimerView(timerManager);
            intermissionLoggerView = new IntermissionLoggerView(mainTimerView);

            mainTimerView.FormClosing += MainTimerView_FormClosing;

            guiRedrawTimer = new WForms.Timer();
            guiRedrawTimer.Interval = 16;
            guiRedrawTimer.Tick += GuiRedrawTimer_Tick;
            guiRedrawTimer.Start();

            soundManager = new SoundManager();

            int y = Screen.PrimaryScreen.WorkingArea.Bottom - mainTimerView.Height - 20;
            int x = Screen.PrimaryScreen.WorkingArea.Right - mainTimerView.Width - 20;
            mainTimerView.StartPosition = FormStartPosition.Manual;
            mainTimerView.Location = new System.Drawing.Point(x, y);

            // NOTHING GETS EXECUTED BELOW THIS CALL
            // NOTHING GETS EXECUTED BELOW THIS CALL
            // NOTHING GETS EXECUTED BELOW THIS CALL
            Application.Run(mainTimerView);
        }

        private static void SetupContextMenu()
        {
            // This is used to get icons on the ContextMenu.
            // It's used really weirdly, see code below.
            VistaMenu vistaMenu = new VistaMenu();

            ContextMenu contextMenu = new ContextMenu();

            var coolDisabledApplicationNameItem = new MenuItem(
                Properties.Settings.Default.AppName 
                + " - " 
                + Properties.Settings.Default.AppVersion);

            coolDisabledApplicationNameItem.Enabled = false;

            vistaMenu.SetImage(coolDisabledApplicationNameItem, Properties.Resources.app_icon.ToBitmap());

            var maximizeApplicationItem = new MenuItem("Open", (sender, args) =>
            {
                mainTimerView.RestoreFromTray();
            });

            maximizeApplicationItem.DefaultItem = true;

            var exitApplicationItem = new MenuItem("Exit", (sender, args) =>
            {
                ExitApplication();
            });

            toggleTimerItem = new MenuItem("Start timer", (sender, args) =>
            {
                if (timerManager.IsStarted)
                    timerManager.Stop();
                else
                    timerManager.Start();
            });

            timerTimeItem = new MenuItem("00:00:00");
            timerTimeItem.Enabled = false;
            timerTimeItem.Visible = false;

            var showConfigItem = new MenuItem("Settings...", (sender, args) =>
            {
                if (configView != null && configView.Visible)
                    return;

                configView = new ConfigView();
                configView.FormClosed += ConfigView_FormClosed;
                configView.ShowDialog(mainTimerView);
            });

            var contextMenuSeparator = "-"; // Lol, this is real

            contextMenu.MenuItems.Add(coolDisabledApplicationNameItem);
            contextMenu.MenuItems.Add(contextMenuSeparator);
            contextMenu.MenuItems.Add(timerTimeItem);
            contextMenu.MenuItems.Add(toggleTimerItem);
            contextMenu.MenuItems.Add(contextMenuSeparator);
            contextMenu.MenuItems.Add(maximizeApplicationItem);
            contextMenu.MenuItems.Add(showConfigItem);
            contextMenu.MenuItems.Add(contextMenuSeparator);
            contextMenu.MenuItems.Add(exitApplicationItem);

            notifyIcon.ContextMenu = contextMenu;

            // This is important too, for reasons.
            ((System.ComponentModel.ISupportInitialize)(vistaMenu)).EndInit();
        }

        private static void SetupNotifyIcon()
        {
            notifyIcon = new NotifyIcon();
            notifyIcon.Icon = Properties.Resources.app_icon;
            notifyIcon.Visible = true;
            notifyIcon.DoubleClick += NotifyIcon_DoubleClick;
        }

        #endregion


        #region // ------- Public Events ------- //

        /// <summary>
        /// Fires every 16ms, use as main redraw timer.
        /// </summary>
        internal static event GuiRedrawEventHandler GuiRedraw;

        internal static void OnGuiRedraw(EventArgs args)
        {
            if (GuiRedraw == null)
                return;

            // This null here feels dirty
            GuiRedraw(null, args);
        }

        #endregion


        #region // ------- Private Event Handlers ------- //

        private static void GuiRedrawTimer_Tick(object sender, EventArgs e)
        {
            OnGuiRedraw(e);
        }

        private static void TimerManager_ActiveTimerTick(object sender, ActiveTimerTickEventArgs args)
        {
            if (args.RemainingDurationMilli <= OOT_DELAY_DISPLAY_THRESHOLD_MS
            && args.TimerType == TimerType.Work)
            {
                ShowOotView();
            }

            if (args.RemainingDurationMilli >= OOT_DELAY_DISPLAY_THRESHOLD_MS
            && args.TimerType == TimerType.Work)
            {
                CloseOotView();
            }

            string timerTimeValue = TimeFormatUtils.FormatMilliAsHHMMSS(timerManager.RemainingTimeMilli);
            string timerNameValue = timerManager.ActiveTimerName;
            timerTimeItem.Text = timerNameValue + " - " + timerTimeValue;
        }

        private static void TimerManager_TimerChangedState(object sender, TimerChangedStateArgs args)
        {
            switch (args.TimerState)
            {
                case TimerState.BEFORE_STARTED:
                    break;

                case TimerState.AFTER_STARTED:
                    toggleTimerItem.Text = "Stop timer";
                    timerTimeItem.Visible = true;
                    break;

                case TimerState.BEFORE_STOPPED:
                    CloseOverlay();
                    CloseOotView();
                    break;

                case TimerState.AFTER_STOPPED:
                    toggleTimerItem.Text = "Start timer";
                    timerTimeItem.Visible = false;
                    break;

                case TimerState.BEORE_PAUSED:
                    break;
                case TimerState.AFTER_PAUSED:
                    break;
                case TimerState.BEFORE_RESUMED:
                    break;
                case TimerState.AFTER_RESUMED:
                    break;
                default:
                    throw new ArgumentException("Unhandled case", args.TimerState.ToString());
            }
        }

        private static void TimerManager_ActiveTimerChanged(object sender, ActiveTimerChangedArgs args)
        {
            var timerStarted = args.TimerIsStarted;

            if (timerStarted == false)
                return;

            UpdateOverlayState(args.NextTimerType);
            UpdateOotViewState(args.NextTimerType);
            PlayChangeSound(args.PreviousTimerType);
        }

        private static void MainTimerView_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (config.Close_Button_Exits)
            {
                ExitApplication();
                return;
            }

            if (e.CloseReason != CloseReason.ApplicationExitCall)
            {
                var isRunning = timerManager.IsRunning;
                var activeTimerType = timerManager.ActiveTimerType;

                if (isRunning && activeTimerType == TimerType.Rest)
                {
                    ShowOverlay();
                }

                mainTimerView.MinimizeToTray();
                e.Cancel = true;
            }
        }

        private static void ConfigView_FormClosed(object sender, FormClosedEventArgs e)
        {
            mainTimerView.UpdateExtendTimerButtonsText();
            UpdateAutostartRegKey();
        }

        private static void NotifyIcon_DoubleClick(object sender, EventArgs e)
        {
            if (mainTimerView.Visible == false)
            {
                mainTimerView.RestoreFromTray();
                CloseOotView();
            } else
            {
                mainTimerView.Activate();
            }

            UpdateOverlayState(timerManager.ActiveTimerType);
        }

        #endregion


        #region // ------- Private API ------- //

        private static void ExitApplication()
        {
            config.Work_Duration_String = mainTimerView.GetWorkDurationAsString();
            config.Rest_Duration_String = mainTimerView.GetRestDurationAsString();

            ConfigManager.WriteToDisk();

            // Because apparently asking WinForms to clean
            // up after itself is too much.
            notifyIcon.Dispose();
            Application.Exit();
        }

        private static void UpdateAutostartRegKey()
        {
            if (isDebugMode)
            {
                DebugLogger.Log("Not writing autostart reg key during debug");
                return;
            }

            RegistryKey registryKey = Registry.CurrentUser
                .OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            if (config.Auto_Start_With_Windows)
            {
                registryKey.SetValue(Properties.Settings.Default.AppName, Application.ExecutablePath);
            }
            else
            {
                registryKey.DeleteValue(Properties.Settings.Default.AppName, false);
            }
        }

        private static void ShowOotView()
        {
            if (mainTimerView.Visible == true)
            {
                return;
            }

            if (outOfTimeAlertView != null && outOfTimeAlertView.Visible == true)
            {
                return;
            }
            
            if (config.Show_Out_Of_Time_Alert == false)
            {
                return;
            }

            if (config.Disable_Alert_If_Proc_Active)
            {
                if (ProcessUtils.IsActiveProcessPresentInList(config.OOT_Proc_List))
                {
                    return;
                }
            }

            outOfTimeAlertView = new OutOfTimeAlertView(timerManager);
            outOfTimeAlertView.Show();
        }

        private static void CloseOotView()
        {
            if (outOfTimeAlertView != null && outOfTimeAlertView.Visible == true)
            {
                outOfTimeAlertView.Close();
            }
        }

        private static void UpdateOotViewState(TimerType nextTimerType)
        {
            if (nextTimerType == TimerType.Rest)
            {
                CloseOotView();
            }
        }

        private static void PlayChangeSound(TimerType previousTimerType)
        {
            // If between disabled internval... shhh
            if (config.Disable_Sound_During_Hours)
            {
                var timeNow = DateTime.Now;

                // 07:00 | 10:00 | 09:01
                var fromHoursString = config.Disable_From_Hours;
                var fromTime = TimeSpan.Parse(fromHoursString + ":00");

                // 10:00 | 07:00 | 09:00
                var toHoursString = config.Disable_To_Hours;
                var toTime = TimeSpan.Parse(toHoursString + ":00");

                var nowTime = DateTime.Now.TimeOfDay;

                if (fromTime < toTime)
                {
                    // Normal logic is reliable, simple after-start / before-end
                    if (nowTime >= fromTime && nowTime <= toTime)
                    {
                        return;
                    }
                } 
                else
                {
                    // Confusing logic is required
                    if (nowTime >= fromTime || nowTime <= toTime)
                    {
                        return;
                    }
                }

            }

            if (timerManager.IsRunning)
            {
                if (previousTimerType == TimerType.Work && config.Play_Sound_Work_Ends)
                {
                    soundManager.PlaySwitchSound(TimerType.Rest);
                    return;
                }
                else if (previousTimerType == TimerType.Rest && config.Play_Sound_Rest_Ends)
                {
                    soundManager.PlaySwitchSound(TimerType.Work);
                    return;
                }
            }
        }

        private static void UpdateOverlayState(TimerType timerType)
        {
            if (timerType == TimerType.Work)
            {
                CloseOverlay();
                return;
            }

            if (mainTimerView.Visible)
            {
                CloseOverlay();
                return;
            }

            ShowOverlay();
        }

        private static void ShowOverlay()
        {
            if (config.Show_Overlay_During_Rest == false)
            {
                return;
            }

            if (config.Disable_Overlay_If_Proc_Active)
            {
                List<string> processList;
                if (config.Use_OOT_Proc_List_For_Overlay)
                {
                    processList = config.OOT_Proc_List;
                }
                else
                {
                    processList = config.Overlay_Proc_List;
                }

                if (ProcessUtils.IsActiveProcessPresentInList(processList))
                {
                    return;
                }
            }

            // We're creating a new one because Close() triggers
            // Dispose() and it all gets messy, cba to hide it
            // maintain state.
            switch (config.Overlay_Type)
            {
                case OverlayType.FULLSCREEN:
                    overlayForm = new OverlayFullView(timerManager);
                    break;
                case OverlayType.BIG_WINDOWED:
                    overlayForm = new OverlayMediumView(timerManager);
                    break;
                case OverlayType.SMALL_WINDOWED:
                    throw new ArgumentException("Small windowed not yet implemented");
                default:
                    throw new ArgumentException("Unhandled case", config.Overlay_Type.ToString());
            }


            overlayForm.Show(mainTimerView);
        }

        private static void CloseOverlay()
        {
            if (overlayForm != null && overlayForm.Visible == true)
            {
                overlayForm.Close();
            }
        }

        private static void ShowModalIntermissionLogger(Form parentForm)
        {
            intermissionLoggerView.ShowDialog(parentForm);
        }

        #endregion

    }
}
